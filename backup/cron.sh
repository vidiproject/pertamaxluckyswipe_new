#!/bin/bash
PROJECT_DIR=/var/www
COMMIT_TIMESTAMP=`date +'%Y-%m-%d %H:%M:%S %Z'`
DATELOG=`date +'%Y-%m-%d'`

# Database credentials
user="db_animatrep"
password="a0x@7t3\$IzyY"
host="localhost"
db_name="db_animatrep"
# Other options
backup_path="${PROJECT_DIR}/backup/database"


echo '---------------------------'
echo 'Doing Git Commit'
echo '----------------------------'

echo "Moving to ${PROJECT_DIR}"

cd $PROJECT_DIR;



# BACKUP PROCESS
echo '---------------------------'
echo 'Doing MYQL BACKUP'
echo '----------------------------'

echo "Doing Backup MYSQL on ${backup_path}/backup-${db_name}-${DATELOG}.sql"
echo ''

if [ ! -d $backup_path ]; then
    mkdir -p $backup_path
fi

mysqldump --user=$user --password=$password --host=$host $db_name > $backup_path/backup-$db_name-$DATELOG.sql

echo ''
echo 'Doing GIT Proccess ....'

LOCK="${PROJECT_DIR}/.git/index.lock"
if [ -f $LOCK ]; then
   rm -f $LOCK
fi

git add .
git commit -am "Auto Backup on ${COMMIT_TIMESTAMP}" &2>/dev/null 
git push origin master > /dev/null


exit

