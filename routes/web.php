<?php

use Jenssegers\Agent\Agent;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::group(['middleware' => ['forceSSL']], function () {

Route::get('/', 'WelcomeController@index');
Route::get('/intro', 'WelcomeController@intro');
Route::get('/mekanisme', 'WelcomeController@mekanisme');
Route::get('/hadiah', 'WelcomeController@hadiah');
Route::get('/syarat-dan-ketentuan', 'WelcomeController@tnc');
Route::get('/faq', 'WelcomeController@faq');

Auth::routes();

});

// Customer Group
Route::group(['middleware' => ['auth','role.moderator','role.admin', 'forceSSL']], function () {
    Route::get('/home', 'HomeController@index');
    Route::get('/thank-you', 'HomeController@getThanks');
    Route::get('/home/upload-struk', 'HomeController@getUploadStruk');
    Route::post('/home/upload-struk-store', 'HomeController@postUploadStrukStore');
    Route::get('/home/upload-struk-store', 'WelcomeController@error'); // error
    Route::get('/struk-uploaded', 'HomeController@getUploaded');
    Route::post('/home/upload-struk-a', 'HomeController@upload_struk_a');
    Route::post('/home/upload-struk-b', 'HomeController@upload_struk_b');
    Route::get('/home/upload-struk-a', 'WelcomeController@error'); // error
    Route::get('/home/upload-struk-b', 'WelcomeController@error'); // error
    //Route::post('/home/upload-test', 'HomeController@upload_test');
    Route::get('/home/edit', 'HomeController@edit');
    Route::post('/home/edit', 'HomeController@update');
});

// Admin Group
Route::group(['middleware' => ['auth','role.moderator','role.customer', 'forceSSL']], function () {
    Route::get('/admin', 'Admin\AdminHome@index');

    Route::get('/admin/receipt', 'Admin\AdminHome@total_receipt');
    Route::post('/admin/receipt', 'Admin\AdminHome@total_receipt_search');
    Route::get('/admin/receipt/edit/{id}', 'Admin\AdminHome@edit_receipt');
    Route::post('/admin/receipt/update', 'Admin\AdminHome@update_receipt');

    // trash receipts
    Route::get('/admin/receipt/junk', 'Admin\AdminHome@junk');
    Route::post('/admin/receipt/junk/delete', 'Admin\AdminHome@junkDelete');

    Route::get('/admin/point/edit/{id}', 'Admin\AdminHome@edit_point'); // points
    Route::post('/admin/point/update', 'Admin\AdminHome@update_point'); // points
    Route::get('/admin/export/receipts/{bank}', 'Admin\AdminHome@exportReceipt');
    Route::get('/admin/user', 'Admin\AdminUser@index');
    Route::get('/admin/user/customer', 'Admin\AdminUser@customer');
    Route::post('/admin/user/customer', 'Admin\AdminUser@searchCustomer');
    Route::get('/admin/user/customer/{id}', 'Admin\AdminUser@viewCustomer');
    Route::get('/admin/user/edit/{id}', 'Admin\AdminUser@getEdit');
    Route::post('/admin/user/update', 'Admin\AdminUser@postUpdate');

    Route::get('/admin/user/moderator', 'Admin\AdminUser@moderator');

    // khusus :D
    Route::get('/admin/page/struk', 'Admin\AdminHome@page_struk');
    Route::get('/admin/page/struk-lagi', 'Admin\AdminHome@page_struk2');
    Route::get('/admin/page/report', 'Admin\AdminHome@report');
});

// moderator
Route::group(['middleware' => ['auth','role.customer','role.admin', 'forceSSL']], function () {

    Route::get('/moderator/participant', 'Moderator\ModeratorHome@participant');
    Route::post('/moderator/participant', 'Moderator\ModeratorHome@participantSearch');
    Route::get('/moderator/participant/{id}', 'Moderator\ModeratorHome@participantFind');

    Route::get('/moderator/data', 'Moderator\ModeratorHome@data');
    Route::post('/moderator/data', 'Moderator\ModeratorHome@dataPost');

    Route::get('/moderator', 'Moderator\ModeratorHome@index');
    Route::get('/moderator/check', 'Moderator\ModeratorHome@check');
    Route::post('/moderator/check', 'Moderator\ModeratorHome@checkSearch');
    Route::get('/moderator/check/edit/{id}', 'Moderator\ModeratorHome@edit');
    Route::post('/moderator/update', 'Moderator\ModeratorHome@update');

    Route::get('/moderator/approval', 'Moderator\ModeratorHome@approval_page');
    Route::post('/moderator/approval', 'Moderator\ModeratorHome@approval_pageSearch');

    Route::get('/moderator/approval/eight', 'Moderator\ModeratorHome@approval_above_eight');
    Route::post('/moderator/approval/eight', 'Moderator\ModeratorHome@approval_above_eightSearch');

    Route::get('/moderator/approval/approved', 'Moderator\ModeratorHome@approved');
    Route::post('/moderator/approval/approved', 'Moderator\ModeratorHome@approvedSearch');

    Route::get('/moderator/approval/declined', 'Moderator\ModeratorHome@declined');

    Route::post('/moderator/approval/approve', 'Moderator\ModeratorHome@approve');
    Route::post('/moderator/approval/decline', 'Moderator\ModeratorHome@decline');

    Route::post('/moderator/receipt/reset', 'Moderator\ModeratorHome@reset');
    Route::post('/moderator/receipt/delete', 'Moderator\ModeratorHome@delete');

    //Route::get('/moderator/receipt/approve/all', 'Moderator\ModeratorHome@approveAll');

    Route::get('/moderator/export/{type}/{status}', 'Moderator\ModeratorHome@export');
    Route::get('/moderator/export/eight', 'Moderator\ModeratorHome@exportEight');
    Route::get('/moderator/participant/export/{type}', 'Moderator\ModeratorHome@exportUsers');
    
    
});
