<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\Point;
use App\Receipt;

use Validator;
use Auth;

class AdminUser extends Controller
{
    public function index()
    {
      $users = User::orderBy('role', 'DESC')->paginate(50);
      return view('admin.user.home')
        ->with('users', $users);
    }

    public function customer()
    {
      $users = User::where('role', 0)->latest()->paginate(50);
      return view('admin.user.customer')
        ->with('users', $users);
    }

    // search customer by name / email
    public function searchCustomer(Request $request)
    {
      $users = User::where('role', 0)
        ->where('name', 'like', '%'.$request->input('key') .'%')
        ->orWhere('email', 'like', '%'.$request->input('key') .'%')
        ->paginate(50);
      return view('admin.user.customer')
        ->with('users', $users);
    }

    // get user detail
    public function viewCustomer($id)
    {
      $user = User::find($id);
      $receipts = Receipt::where('user_id', $id)->paginate(20);
      $points = Point::where('user_id', $id)->paginate(20);
      return view('admin.user.customer-view')
        ->with('receipts', $receipts)
        ->with('points', $points)
        ->with('user', $user);
    }

    // edit user
    public function getEdit($id)
    {
      $user = User::find($id);
      return view('admin.user.edit')
        ->with('user', $user);
    }

    // update data user
    public function postUpdate(Request $request)
    {
      $valid = Validator::make($request->all(), [
        'name' => 'required',
        'email' => 'required|unique:users,email,'. $request->input('id'),
        'role' => 'required',
        'bank' => 'required_if:role,2',
        'ktp'   => 'required|digits:16',
        'phone'   => 'required|numeric',
      ]);

      if ($valid->fails()) {
        return redirect()->back()
          ->withInput()
          ->withErrors($valid);
      } else {

        $user = User::find($request->input('id'));
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->ktp = $request->input('ktp');
        $user->phone = $request->input('phone');
        $user->role = $request->input('role');
        if ($request->input('role') == 2) {
          $user->handle = $request->input('bank');
        } else {
          $user->handle = null;
        }

        if ($user->save()) {
          return redirect($request->input('previous_url'))
            ->with('message', 'User has been updated');
        } else {
          return redirect()->back()
            ->with('error', 'Failed. Please try again');
        }

      }
    }


    // moderator page
    public function moderator()
    {
      $moderators = User::where('role', 2)->get();
      return view('admin.user.moderator.home')
        ->with('moderators', $moderators);
    }
}
