<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\Receipt;
use App\Point;
use App\Bin;

use Excel;
use Validator;
use Auth;
use DB; 

class AdminHome extends Controller
{
    public function index()
    {

        // if not me :D
        if (Auth::user()->email != 'rahmankurnia1126@gmail.com') {

          $total_registration = User::where('role', 0)->count();
          // $unique_registration = User::where('users.role', 0)
          //   ->has('receipt')
          //   ->get();
          $unique_registration = Receipt::leftJoin('users', 'users.id', 'receipts.user_id')
            ->select('users.*', 'receipts.nospbu')
            ->where('users.role', 0)
            ->groupBy('users.id')
            ->get()->count();
          $submision = Receipt::where('status', '<>', 2)->count();
          // jenis bensin
          $pertamax = Receipt::where('status', '<>', 2)->where('bensin', 'pertamax')->count();
          $pertamax_turbo = Receipt::where('status', '<>', 2)->where('bensin', 'pertamax turbo')->count();
          $pertamina_dex = Receipt::where('status', '<>', 2)->where('bensin', 'pertamina dex')->count();
          $pertalite = Receipt::where('status', '<>', 2)->where('bensin', 'pertalite')->count();
          $dexlite = Receipt::where('status', '<>', 2)->where('bensin', 'dexlite')->count();

          // by locations
          $daerah = Receipt::select('receipts.*', 'spbu.*', DB::raw('count(receipts.id) as total'))
            ->leftJoin('spbu', 'receipts.nospbu', '=', 'spbu.id')
            ->where('receipts.status', '<>', 2)
            ->groupBy('spbu.provinsi')
            ->orderBy('spbu.id', 'DESC')
            ->orderBy('total', 'DESC')            
            ->paginate(8);

          return view('admin.p-home')
            ->with('total_users', $total_registration)
            ->with('unique_users', $unique_registration)
            ->with('pertamax', $pertamax)
            ->with('pertamax_turbo', $pertamax_turbo)
            ->with('pertamina_dex', $pertamina_dex)
            ->with('pertalite', $pertalite)
            ->with('dexlite', $dexlite)
            ->with('submision', $submision)
            ->with('daerah', $daerah);

        } else {

        	$admins = User::where('role', 1)->count();
        	$moderators = User::where('role', 2)->count();
        	$users = User::where('role', 0)->count();
        	$receipts = Receipt::where('namabank', '<>', null)->count();
          $receipts_count = Receipt::where('status', '<>', 2)->avg('nominal');
        	$receipts_approved = Receipt::where('status', 1)->count();
        	$receipts_disapproved = Receipt::where('status', 2)->count();
        	$receipts_bni = Receipt::where('namabank', 'bni')->count();
        	$receipts_bri = Receipt::where('namabank', 'bri')->count();
        	$receipts_mandiri = Receipt::where('namabank', 'mandiri')->count();
          $receipts_bni_approved = Receipt::where('namabank', 'bni')->where('status', 1)->count();
          $receipts_bri_approved = Receipt::where('namabank', 'bri')->where('status', 1)->count();
          $receipts_mandiri_approved = Receipt::where('namabank', 'mandiri')->where('status', 1)->count();
          	return view('admin.home')
          		->with('receipts', $receipts)
          		->with('receipts_approved', $receipts_approved)
          		->with('receipts_disapproved', $receipts_disapproved)
          		->with('admins', $admins)
          		->with('moderators', $moderators)
          		->with('receipts_bni', $receipts_bni)
          		->with('receipts_bri', $receipts_bri)
          		->with('receipts_mandiri', $receipts_mandiri)
              ->with('receipts_bni_approved', $receipts_bni_approved)
              ->with('receipts_bri_approved', $receipts_bri_approved)
              ->with('receipts_mandiri_approved', $receipts_mandiri_approved)
              ->with('receipts_count', $receipts_count)
          		->with('users', $users);

        }
    }

    // halaman struk
    public function page_struk(Request $request)
    {
      if ($request->query('from') == null || $request->query('to') == null) {

        $total_mandiri = Receipt::where('namabank', 'mandiri')
          ->where('status', '<>', 2)
          ->count();
        $total_mandiri_debit = Receipt::where('namabank', 'mandiri')
          ->where('status', '<>', 2)
          ->where('pembayaran', 'debit')->count();
        $total_mandiri_debit_approve = Receipt::where('namabank', 'mandiri')
          ->where('pembayaran', 'debit')
          ->where('status', 1)
          ->count();
        $total_mandiri_debit_inapprove = Receipt::where('namabank', 'mandiri')
          ->where('pembayaran', 'debit')
          ->where('status', 0)
          ->count();

        $total_mandiri_kredit = Receipt::where('namabank', 'mandiri')
          ->where('status', '<>', 2)
          ->where('pembayaran', 'kredit')
          ->count();
        $total_mandiri_kredit_approve = Receipt::where('namabank', 'mandiri')
          ->where('pembayaran', 'kredit')
          ->where('status', 1)
          ->count();
        $total_mandiri_kredit_inapprove = Receipt::where('namabank', 'mandiri')
          ->where('pembayaran', 'kredit')
          ->where('status', 0)
          ->count();

        $total_bri = Receipt::where('namabank', 'bri')->where('status', '<>', 2)->count();
        $total_bri_debit = Receipt::where('namabank', 'bri')
          ->where('status', '<>', 2)
          ->where('pembayaran', 'debit')->count();
        $total_bri_debit_approve = Receipt::where('namabank', 'bri')
          ->where('pembayaran', 'debit')
          ->where('status', 1)
          ->count();
        $total_bri_debit_inapprove = Receipt::where('namabank', 'bri')
          ->where('pembayaran', 'debit')
          ->where('status', 0)
          ->count();
        $total_bri_kredit = Receipt::where('namabank', 'bri')
          ->where('status', '<>', 2)
          ->where('pembayaran', 'kredit')->count();
        $total_bri_kredit_approve = Receipt::where('namabank', 'bri')
          ->where('pembayaran', 'kredit')
          ->where('status', 1)
          ->count();
        $total_bri_kredit_inapprove = Receipt::where('namabank', 'bri')
          ->where('pembayaran', 'kredit')
          ->where('status', 0)
          ->count();

        $total_bni = Receipt::where('namabank', 'bni')->where('status', '<>', 2)->count();
        $total_bni_debit = Receipt::where('namabank', 'bni')
          ->where('status', '<>', 2)
          ->where('pembayaran', 'debit')->count();
        $total_bni_debit_approve = Receipt::where('namabank', 'bni')
          ->where('pembayaran', 'debit')
          ->where('status', 1)
          ->count();
        $total_bni_debit_inapprove = Receipt::where('namabank', 'bni')
          ->where('pembayaran', 'debit')
          ->where('status', 0)
          ->count();
        $total_bni_kredit = Receipt::where('namabank', 'bni')
          ->where('status', '<>', 2)
          ->where('pembayaran', 'kredit')->count();
        $total_bni_kredit_approve = Receipt::where('namabank', 'bni')
          ->where('pembayaran', 'kredit')
          ->where('status', 1)
          ->count();
        $total_bni_kredit_inapprove = Receipt::where('namabank', 'bni')
          ->where('pembayaran', 'kredit')
          ->where('status', 0)
          ->count();
        
      } else {

        $total_mandiri = Receipt::where('namabank', 'mandiri')->where('status', '<>', 2)
          ->whereBetween('created_at', [$request->query('from'), $request->query('to')])
          ->count();
        $total_mandiri_debit = Receipt::where('namabank', 'mandiri')
          ->where('pembayaran', 'debit')
          ->where('status', '<>', 2)
          ->whereBetween('created_at', [$request->query('from'), $request->query('to')])
          ->count();
        $total_mandiri_debit_approve = Receipt::where('namabank', 'mandiri')
          ->where('pembayaran', 'debit')
          ->where('status', 1)
          ->whereBetween('created_at', [$request->query('from'), $request->query('to')])
          ->count();
        $total_mandiri_debit_inapprove = Receipt::where('namabank', 'mandiri')
          ->where('pembayaran', 'debit')
          ->where('status', 0)
          ->whereBetween('created_at', [$request->query('from'), $request->query('to')])
          ->count();

        $total_mandiri_kredit = Receipt::where('namabank', 'mandiri')
          ->whereBetween('created_at', [$request->query('from'), $request->query('to')])
          ->where('pembayaran', 'kredit')
          ->where('status', '<>', 2)
          ->count();
        $total_mandiri_kredit_approve = Receipt::where('namabank', 'mandiri')
          ->whereBetween('created_at', [$request->query('from'), $request->query('to')])
          ->where('pembayaran', 'kredit')
          ->where('status', 1)
          ->count();
        $total_mandiri_kredit_inapprove = Receipt::where('namabank', 'mandiri')
          ->whereBetween('created_at', [$request->query('from'), $request->query('to')])
          ->where('pembayaran', 'kredit')
          ->where('status', 0)
          ->count();

        $total_bri = Receipt::where('namabank', 'bri')
          ->where('status', '<>', 2)
          ->whereBetween('created_at', [$request->query('from'), $request->query('to')])
          ->count();
        $total_bri_debit = Receipt::where('namabank', 'bri')
          ->where('pembayaran', 'debit')
          ->where('status', '<>', 2)
          ->whereBetween('created_at', [$request->query('from'), $request->query('to')])
          ->count();
        $total_bri_debit_approve = Receipt::where('namabank', 'bri')
          ->where('pembayaran', 'debit')
          ->where('status', 1)
          ->whereBetween('created_at', [$request->query('from'), $request->query('to')])
          ->count();
        $total_bri_debit_inapprove = Receipt::where('namabank', 'bri')
          ->where('pembayaran', 'debit')
          ->where('status', 0)
          ->whereBetween('created_at', [$request->query('from'), $request->query('to')])
          ->count();

        $total_bri_kredit = Receipt::where('namabank', 'bri')
          ->where('status', '<>', 2)
          ->whereBetween('created_at', [$request->query('from'), $request->query('to')])
          ->where('pembayaran', 'kredit')
          ->count();
        $total_bri_kredit_approve = Receipt::where('namabank', 'bri')
          ->whereBetween('created_at', [$request->query('from'), $request->query('to')])
          ->where('pembayaran', 'kredit')
          ->where('status', 1)
          ->count();
        $total_bri_kredit_inapprove = Receipt::where('namabank', 'bri')
          ->whereBetween('created_at', [$request->query('from'), $request->query('to')])
          ->where('pembayaran', 'kredit')
          ->where('status', 0)
          ->count();


        $total_bni = Receipt::where('namabank', 'bni')
          ->where('status', '<>', 2)
          ->whereBetween('created_at', [$request->query('from'), $request->query('to')])
          ->count();

        $total_bni_debit = Receipt::where('namabank', 'bni')
          ->where('pembayaran', 'debit')
          ->where('status', '<>', 2)
          ->whereBetween('created_at', [$request->query('from'), $request->query('to')])
          ->count();
        $total_bni_debit_approve = Receipt::where('namabank', 'bni')
          ->where('pembayaran', 'debit')
          ->where('status', 1)
          ->whereBetween('created_at', [$request->query('from'), $request->query('to')])
          ->count();
        $total_bni_debit_inapprove = Receipt::where('namabank', 'bni')
          ->where('pembayaran', 'debit')
          ->where('status', 0)
          ->whereBetween('created_at', [$request->query('from'), $request->query('to')])
          ->count();

        $total_bni_kredit = Receipt::where('namabank', 'bni')
          ->where('pembayaran', 'kredit')
          ->where('status', '<>', 2)
          ->whereBetween('created_at', [$request->query('from'), $request->query('to')])
          ->count();
        $total_bni_kredit_approve = Receipt::where('namabank', 'bni')
          ->where('pembayaran', 'kredit')
          ->where('status', 1)
          ->whereBetween('created_at', [$request->query('from'), $request->query('to')])
          ->count();
        $total_bni_kredit_inapprove = Receipt::where('namabank', 'bni')
          ->where('pembayaran', 'kredit')
          ->where('status', 0)
          ->whereBetween('created_at', [$request->query('from'), $request->query('to')])
          ->count();

      }

      return view('admin.p-struk')
        ->with('total_mandiri', $total_mandiri)
        ->with('total_mandiri_debit', $total_mandiri_debit)
        ->with('total_mandiri_debit_approve', $total_mandiri_debit_approve)
        ->with('total_mandiri_debit_inapprove', $total_mandiri_debit_inapprove)
        ->with('total_mandiri_kredit', $total_mandiri_kredit)
        ->with('total_mandiri_kredit_approve', $total_mandiri_kredit_approve)
        ->with('total_mandiri_kredit_inapprove', $total_mandiri_kredit_inapprove)
        ->with('total_bri', $total_bri)
        ->with('total_bri_debit', $total_bri_debit)
        ->with('total_bri_debit_approve', $total_bri_debit_approve)
        ->with('total_bri_debit_inapprove', $total_bri_debit_inapprove)
        ->with('total_bri_kredit', $total_bri_kredit)
        ->with('total_bri_kredit_approve', $total_bri_kredit_approve)
        ->with('total_bri_kredit_inapprove', $total_bri_kredit_inapprove)
        ->with('total_bni', $total_bni)
        ->with('total_bni_debit', $total_bni_debit)
        ->with('total_bni_debit_approve', $total_bni_debit_approve)
        ->with('total_bni_debit_inapprove', $total_bni_debit_inapprove)
        ->with('total_bni_kredit', $total_bni_kredit)
        ->with('total_bni_kredit_approve', $total_bni_kredit_approve)
        ->with('total_bni_kredit_inapprove', $total_bni_kredit_inapprove);
    }


    public function page_struk2(Request $request){

      $startDate = $request->input('startDate', '2017-01-30');
      $endDate = $request->input('endDate', '2017-02-30');

      $total = User::whereBetween('created_at', [$startDate, $endDate])
          ->has('receipt')
          //->where('created_at', '<>', 'updated_at')
          ->groupBy('date')
          ->orderBy('date', 'ASC')
          //->remember(1440)
          ->get([
              DB::raw('Date(created_at) as date'),
              DB::raw('COUNT(*) as value')
          ])
          ->toJSON();

      // $total = DB::table('receipts')
      //   ->leftJoin('users', 'users.id', 'receipts.user_id')
      //   ->where('receipts.status', '<>', 2)
      //   ->whereBetween('receipts.created_at', [$startDate, $endDate])
      //   ->selectRaw('receipts.created_at, receipts.user_id, Date(receipts.created_at) as date, count(distinct receipts.user_id) as value')
      //   ->groupBy('date')
      //   ->get()->toJSON();

      // $unique_registration = Receipt::leftJoin('users', 'users.id', 'receipts.user_id')
      //       ->select('users.*', 'receipts.nospbu')
      //       ->where('users.role', 0)
      //       ->groupBy('users.id')
      //       ->get();

      // mandiri
      $total_mandiri = DB::table('receipts')
        ->where('namabank', 'mandiri')
        ->where('status', '<>', 2)
        ->whereBetween('created_at', [$startDate, $endDate])
        ->selectRaw('Date(created_at) as date, count(DISTINCT user_id) as value')
        ->groupBy('date')->get()->toJSON();

      // mandiri debit
      $total_mandiri_debit = DB::table('receipts')
        ->where('status', '<>', 2)
        ->where('namabank', 'mandiri')
        ->where('pembayaran', 'debit')
        ->whereBetween('created_at', [$startDate, $endDate])
        ->selectRaw('Date(created_at) as date, count(DISTINCT user_id) as value')
        ->groupBy('date')->get()->toJSON();

      // mandiri kredit
      $total_mandiri_kredit = DB::table('receipts')
        ->where('status', '<>', 2)
        ->where('namabank', 'mandiri')
        ->where('pembayaran', 'kredit')
        ->whereBetween('created_at', [$startDate, $endDate])
        ->selectRaw('Date(created_at) as date, count(DISTINCT user_id) as value')
        ->groupBy('date')->get()->toJSON();


      // BNI
      $total_bni = DB::table('receipts')->where('namabank', 'bni')
        ->where('status', '<>', 2)
        ->whereBetween('created_at', [$startDate, $endDate])
        ->selectRaw('Date(created_at) as date, count(DISTINCT user_id) as value')
        ->groupBy('date')->get()->toJSON();

      // BNI debit
      $total_bni_debit = DB::table('receipts')
        ->where('namabank', 'bni')
        ->where('pembayaran', 'debit')
        ->where('status', '<>', 2)
        ->whereBetween('created_at', [$startDate, $endDate])
        ->selectRaw('Date(created_at) as date, count(DISTINCT user_id) as value')
        ->groupBy('date')->get()->toJSON();

      // BNI kredit
      $total_bni_kredit = DB::table('receipts')
        ->where('namabank', 'bni')
        ->where('pembayaran', 'kredit')
        ->where('status', '<>', 2)
        ->whereBetween('created_at', [$startDate, $endDate])
        ->selectRaw('Date(created_at) as date, count(DISTINCT user_id) as value')
        ->groupBy('date')->get()->toJSON();


      // BRI
      $total_bri = DB::table('receipts')->where('namabank', 'bri')
        ->where('status', '<>', 2)
        ->whereBetween('created_at', [$startDate, $endDate])
        ->selectRaw('Date(created_at) as date, count(DISTINCT user_id) as value')
        ->groupBy('date')->get()->toJSON();

      // BNI debit
      $total_bri_debit = DB::table('receipts')
        ->where('namabank', 'bri')
        ->where('pembayaran', 'debit')
        ->where('status', '<>', 2)
        ->whereBetween('created_at', [$startDate, $endDate])
        ->selectRaw('Date(created_at) as date, count(DISTINCT user_id) as value')
        ->groupBy('date')->get()->toJSON();

      // BRI kredit
      $total_bri_kredit = DB::table('receipts')
        ->where('namabank', 'bri')
        ->where('pembayaran', 'kredit')
        ->where('status', '<>', 2)
        ->whereBetween('created_at', [$startDate, $endDate])
        ->selectRaw('Date(created_at) as date, count(DISTINCT user_id) as value')
        ->groupBy('date')->get()->toJSON();

      return view('admin.p-struk2')
        ->with(compact('total', $total))
        ->with(compact('total_mandiri', $total_mandiri))
        ->with(compact('total_mandiri_kredit', $total_mandiri_kredit))
        ->with(compact('total_mandiri_debit', $total_mandiri_debit))
        ->with(compact('total_bri', $total_bri))
        ->with(compact('total_bri_kredit', $total_bri_kredit))
        ->with(compact('total_bri_debit', $total_bri_debit))
        ->with(compact('total_bni', $total_bni))
        ->with(compact('total_bni_kredit', $total_bni_kredit))
        ->with(compact('total_bni_debit', $total_bni_debit));
    }

    // report
    public function report()
    {
      $total_mandiri = Receipt::where('namabank', 'mandiri')->where('status','<>', 2)->sum('nominal');
      $total_mandiri_debit = Receipt::where('namabank', 'mandiri')->where('status','<>', 2)->where('pembayaran', 'debit')->sum('nominal');
        $total_mandiri_debit_approve = Receipt::where('namabank', 'mandiri')
          ->where('status', 1)
          ->where('pembayaran', 'debit')
          ->sum('nominal');
        $total_mandiri_debit_inapprove = Receipt::where('namabank', 'mandiri')
          ->where('status', 0)
          ->where('pembayaran', 'debit')
          ->sum('nominal');
      $total_mandiri_kredit = Receipt::where('namabank', 'mandiri')->where('status','<>', 2)->where('pembayaran', 'kredit')->sum('nominal');
        $total_mandiri_kredit_approve = Receipt::where('namabank', 'mandiri')
          ->where('status', 1)
          ->where('pembayaran', 'kredit')
          ->sum('nominal');
        $total_mandiri_kredit_inapprove = Receipt::where('namabank', 'mandiri')
          ->where('status', 0)
          ->where('pembayaran', 'kredit')
          ->sum('nominal');

      $total_bni = Receipt::where('namabank', 'bni')->where('status','<>', 2)->sum('nominal');
      $total_bni_debit = Receipt::where('namabank', 'bni')->where('status','<>', 2)->where('pembayaran', 'debit')->sum('nominal');
      $total_bni_debit_approve = Receipt::where('namabank', 'bni')
        ->where('status', 1)
        ->where('pembayaran', 'debit')
        ->sum('nominal');
      $total_bni_debit_inapprove = Receipt::where('namabank', 'bni')
        ->where('status', 0)
        ->where('pembayaran', 'debit')
        ->sum('nominal');
      $total_bni_kredit = Receipt::where('namabank', 'bni')->where('status','<>', 2)->where('pembayaran', 'kredit')->sum('nominal');
      $total_bni_kredit_approve = Receipt::where('namabank', 'bni')
        ->where('status', 1)
        ->where('pembayaran', 'kredit')
        ->sum('nominal');
      $total_bni_kredit_inapprove = Receipt::where('namabank', 'bni')
        ->where('status', 0)
        ->where('pembayaran', 'kredit')
        ->sum('nominal');

      $total_bri = Receipt::where('namabank', 'bri')->where('status','<>', 2)->sum('nominal');
      $total_bri_debit = Receipt::where('namabank', 'bri')->where('status','<>', 2)->where('pembayaran', 'debit')->sum('nominal');
      $total_bri_debit_approve = Receipt::where('namabank', 'bri')
        ->where('status', 1)
        ->where('pembayaran', 'debit')
        ->sum('nominal');
      $total_bri_debit_inapprove = Receipt::where('namabank', 'bri')
        ->where('status', 0)
        ->where('pembayaran', 'debit')
        ->sum('nominal');
      $total_bri_kredit = Receipt::where('namabank', 'bri')->where('status','<>', 2)->where('pembayaran', 'kredit')->sum('nominal');
      $total_bri_kredit_approve = Receipt::where('namabank', 'bri')
        ->where('status', 1)
        ->where('pembayaran', 'kredit')
        ->sum('nominal');
      $total_bri_kredit_inapprove = Receipt::where('namabank', 'bri')
        ->where('status', 0)
        ->where('pembayaran', 'kredit')
        ->sum('nominal');

      return view('admin.p-report')
        ->with('total_mandiri', $total_mandiri)
        ->with('total_mandiri_debit', $total_mandiri_debit)
        ->with('total_mandiri_debit_approve', $total_mandiri_debit_approve)
        ->with('total_mandiri_debit_inapprove', $total_mandiri_debit_inapprove)
        ->with('total_mandiri_kredit', $total_mandiri_kredit)
        ->with('total_mandiri_kredit_approve', $total_mandiri_kredit_approve)
        ->with('total_mandiri_kredit_inapprove', $total_mandiri_kredit_inapprove)
        ->with('total_bni', $total_bni)
        ->with('total_bni_debit', $total_bni_debit)
        ->with('total_bni_debit_approve', $total_bni_debit_approve)
        ->with('total_bni_debit_inapprove', $total_bni_debit_inapprove)
        ->with('total_bni_kredit', $total_bni_kredit)
        ->with('total_bni_kredit_approve', $total_bni_kredit_approve)
        ->with('total_bni_kredit_inapprove', $total_bni_kredit_inapprove)
        ->with('total_bri', $total_bri)
        ->with('total_bri_debit', $total_bri_debit)
        ->with('total_bri_debit_approve', $total_bri_debit_approve)
        ->with('total_bri_debit_inapprove', $total_bri_debit_inapprove)
        ->with('total_bri_kredit', $total_bri_kredit)
        ->with('total_bri_kredit_approve', $total_bri_kredit_approve)
        ->with('total_bri_kredit_inapprove', $total_bri_kredit_inapprove);
    }

    // get receipts list
    public function total_receipt()
    {
      $receipts = Receipt::latest()->paginate(50);
      return view('admin.receipts')
        ->with('receipts', $receipts);
    }

    public function total_receipt_search(Request $request)
    {
      $receipts = Receipt::latest()
        ->where('id', 'like', '%'.$request->input('key') .'%')
        ->paginate(50);
      return view('admin.receipts')
        ->with('receipts', $receipts);
    }

    //edit points
    public function edit_point($id)
    {
      $point = Point::find($id);
      return view('admin.edit-point')
        ->with('point', $point);
    }

    // update point
    public function update_point(Request $request)
    {
      $valid = Validator::make($request->all(), [
        'mandiri_poin' => 'required|numeric',
        'mandiri_chance' => 'required|numeric',
        'bni_poin' => 'required|numeric',
        'bni_chance' => 'required|numeric',
        'bri_poin' => 'required|numeric',
        'bri_chance' => 'required|numeric',
      ]);

      if ($valid->fails()) {
        return redirect()
          ->back()
          ->withInput()
          ->withErrors($valid);
      } else {

        $point = Point::find($request->input('id'));
        $point->mandiri_poin = $request->input('mandiri_poin');
        $point->mandiri_chance = $request->input('mandiri_chance');
        $point->bni_poin = $request->input('bni_poin');
        $point->bni_chance = $request->input('bni_chance');
        $point->bri_poin = $request->input('bri_poin');
        $point->bri_chance = $request->input('bri_chance');

        if ($point->save()) {
          return redirect('admin/user/customer')
            ->with('message','Successfully updated');
        } else {
          return redirect()->back()
            ->with('error', 'Please try again');
        }

      }
    }

    // edit receipt
    public function edit_receipt($id)
    {
      $receipt = Receipt::find($id);
      return view('admin.edit-receipt')
        ->with('receipt', $receipt);
    }

    // update receipt
    public function update_receipt(Request $request)
    {
      $valid = Validator::make($request->all(), [
        'poin' => 'required',
      ]);

      $previous_url = 'admin/receipt';

      if ($request->input('previous_url') != null) {
        $previous_url = $request->input('previous_url');
      }

      if ($valid->fails()) {
        return redirect()->back()
          ->withErrors($valid)
          ->withInput();
      } else {
        $receipt = Receipt::find($request->input('id'));
        $receipt->nominal = $request->input('nominal');
        $receipt->poin = $request->input('poin');
        if ($receipt->save()) {
          return redirect($previous_url)
            ->with('message', 'Successfully saved');
        } else {
          return redirect()->back()
            ->with('error', 'Oops, please try again');
        }
      }
    }


    // export
    public function exportReceipt($bank)
    {

      $users = Receipt::where('receipts.namabank', $bank)
        ->join('users', 'users.id', '=', 'receipts.user_id')
        ->select('receipts.id','users.name', 'users.email', 'users.phone', 'users.ktp', 'receipts.struk1', 'receipts.struk2','receipts.nominal', 'receipts.namabank','receipts.pembayaran','receipts.approvalcode','receipts.nocc as enam_digit_pertama','receipts.lastfourdigits as empat_digit_terakhir', 'receipts.cctype as jenis_kartu','receipts.poin','receipts.nospbu','receipts.bensin as jenis_bensin','receipts.created_at')
        ->oldest()
        ->get();

       // Initialize the array which will be passed into the Excel
        // generator.
        $dataArray = []; 

        // Define the Excel spreadsheet headers
        $dataArray[] = ['id', 'nama','email','phone','ktp', 'struk1', 'struk2', 'nominal', 'nama_bank', 'pembayaran', 'approval_code', 'enam_digit_pertama', 'empat_digit_terakhir', 'jenis_kartu', 'poin', 'nospbu', 'jenis_bensin', 'tanggal'];

        // Convert each member of the returned collection into an array,
        // and append it to the payments array.
        foreach ($users as $payment) {
            $dataArray[] = $payment->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('receipts'. time(), function($excel) use ($dataArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('receipts' . microtime());
            $excel->setDescription('receipt file');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function($sheet) use ($dataArray) {
                $sheet->fromArray($dataArray, null, 'A1', false, false);
            });

        })->download('csv');
    }

    // junk
    public function junk()
    {
      $receipts = Receipt::where('status', 2)->oldest();
      return view('admin.receipt.junk.view')
        ->with('receipts', $receipts);
    }

    // delete junk
    public function junkDelete(Request $request)
    {
      $receipt = Receipt::find($request->input('id'));
      if ($receipt->delete()) {
        $struk1 = explode("/", $receipt->struk1, 4);
        $struk2 = explode("/", $receipt->struk2, 4);
        unlink(public_path($struk1[3]));
        unlink(public_path($struk2[3]));
        return redirect()->back()
          ->with('message', 'Successfully deleted');
      } else {
        return redirect()->back()
          ->with('error', 'Please try again');
      }
    }

}
