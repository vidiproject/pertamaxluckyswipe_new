<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Jenssegers\Agent\Agent;

class WelcomeController extends Controller
{
    public function index()
    {
      $agent = new Agent();
      return view('welcome')->with('agent', $agent);
    }

    public function intro()
    {
      $this->middleware('guest');
      $agent = new Agent();
      return view('intro')->with('agent', $agent);
    }

    // Pages
    public function mekanisme()
    {
      return view('mekanisme');
    }

    public function tnc()
    {
      return view('tnc');
    }

    public function hadiah()
    {
      return view('hadiah');
    }

    public function faq()
    {
      return view('faq');
    }

    public function error()
    {
      return view('errors.404');
    }
}
