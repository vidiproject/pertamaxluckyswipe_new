<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Point;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;

use Jenssegers\Agent\Agent;

use GeoIP;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/thank-you';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $rule = [
            'nama' => 'required|max:100|regex:/^[(a-zA-Z\s)]+$/u',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:3|confirmed',
            'phone' => 'required',
            'ktp'   => 'required|digits:16'
        ];

        $message = [
          'name.required'   => 'Nama wajib di isi',
          'name.max'        => 'Nama tidak boleh lebih dari 100 karakter.',
          'name.regex'      => 'Nama hanya boleh berisi huruf dan spasi.',
          'email.required'  => 'Email wajib di isi',
          'email.email'     => 'Format email salah.',
          'email.unique'    => 'Email sudah digunakan',
          'password.required' => 'Password wajib di isi.',
          'phone.required'    => 'Nomor handphone wajib di isi.',
          'ktp.required'      => 'Nomor KTP wajib di isi.',
          'ktp.digits'        => 'Nomor KTP yang benar adalah 16 angka'        
        ];

        return Validator::make($data, $rule, $message);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['nama'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'phone' => str_replace('-', '', $data['phone']),
            'ktp' => $data['ktp'],
            'ip' => GeoIP::getClientIP(),
        ]);

        Point::create([
          'user_id' => $user->id,
        ]);

        return $user;
    }
}
