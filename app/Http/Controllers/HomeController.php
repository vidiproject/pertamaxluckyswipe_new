<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Receipt;
use App\Point;
use App\Bin;

use Validator;
use Auth;
use Cloudder;
use Image;
use Jenssegers\Agent\Agent;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $receipt_mandiri0 = Receipt::where('user_id', Auth::id())
        ->where(function($query){
          $query->where('namabank','mandiri')
            ->where('status', '<>', 2);
        })->sum('poin');


      $receipt_mandiri1 = Receipt::where('user_id', Auth::id())
          ->where(function($query){
            $query->where('namabank','mandiri')
              ->where('status', 1);
          })->sum('poin');
        

          $receipt_mandiri3 = Receipt::where('user_id', Auth::id())
            ->where(function($query){
              $query->where('namabank','mandiri')
                ->where('status', 0);
            })->sum('poin');
      
      

      $receipt_bri0 = Receipt::where('user_id', Auth::id())
        ->where(function($query){
          $query->where('namabank', 'bri')
          ->where('status', '<>', 2);
        })->sum('poin');

      $receipt_bri1 = Receipt::where('user_id', Auth::id())
          ->where(function($query){
            $query->where('namabank', 'bri')
              ->where('status', 1);
          })->sum('poin');
      $receipt_bri3 = Receipt::where('user_id', Auth::id())
            ->where(function($query){
              $query->where('namabank', 'bri')
                ->where('status', 0);
            })->sum('poin');


      $receipt_bni0 = Receipt::where('user_id', Auth::id())
        ->where(function($query){
          $query->where('namabank', 'bni')
          ->where('status', '<>', 2);
        })->sum('poin');

      $receipt_bni1 = Receipt::where('user_id', Auth::id())
        ->where(function($query){
          $query->where('namabank', 'bni')
            ->where('status', 1);
        })->sum('poin');
      $receipt_bni3 = Receipt::where('user_id', Auth::id())
            ->where(function($query){
              $query->where('namabank', 'bni')
                ->where('status', 0);
            })->sum('poin');
          
          

        return view('home')
          ->with('poin_bri', $receipt_bri0)
          ->with('poin_bni', $receipt_bni0)
          ->with('poin_mandiri', $receipt_mandiri0)
          ->with('poin_bri1', $receipt_bri1)
          ->with('poin_bni1', $receipt_bni1)
          ->with('poin_mandiri1', $receipt_mandiri1)
          ->with('poin_bri3', $receipt_bri3)
          ->with('poin_bni3', $receipt_bni3)
          ->with('poin_mandiri3', $receipt_mandiri3);
    }

    public function getThanks()
    {
      return view('thank-you');
    }

    // tambah struk
    public function getUploadStruk()
    {
      return view('upload-struk');
    }

    // edit profile
    public function edit()
    {
      $user = Auth::user();
      return view('edit')->with('user', $user);
    }

    // update data
    public function update(Request $request)
    {

        $rule = [
            'name' => 'required|max:100|regex:/^[(a-zA-Z\s)]+$/u',
            'email' => 'required|email|max:255|unique:users,email,'. $request->input('id'),
        ];

        $message = [
          'name.required'   => 'Nama wajib di isi',
          'name.max'        => 'Nama tidak boleh lebih dari 100 karakter.',
          'name.regex'      => 'Nama hanya boleh berisi huruf dan spasi.',
          'email.required'  => 'Email wajib di isi',
          'email.email'     => 'Format email salah.',
          'email.unique'    => 'Email sudah digunakan',      
        ];

        $valid = Validator::make($request->all(), $rule, $message);

        if ($valid->fails()) {
          return redirect()->back()
            ->withInput()
            ->withErrors($valid);
        } else {

          $user = User::find($request->input('id'));
          $user->name = $request->input('name');
          $user->email = $request->input('email');

          if ($user->save()) {
            return redirect('/home')
              ->with('message', 'Data profile berhasil dirubah');
          } else{
            return redirect()->back()
              ->with('error', 'Gagal meyimpan, silahkan coba lagi');
          }

        }
    }

    // upload struk & data
    public function postUploadStrukStore(Request $request)
    {
      $rule = [
        'bank' => 'required',
        'payment' => 'required',
        'nocc' => 'required|numeric',
        'nominal' => 'required|numeric',
        'nospbu'  => 'required|numeric|regex:/^[0-9]+$/',
      ];

      // regex:/^[0-9]{1,3}(,[0-9]{3})*(\.[0-9]+)*$/ buat validasi titik

      $message = [
          'bank.required' => 'Pilih bank dari kartu debit atau kartu kredit anda',
          'payment.required' => 'Pilih jenis pembayaran',
          'nocc.required_if' => '6 digit pertama pada kartu kredit yang anda gunakan wajib di isi',
          'nominal.required' => 'Masukan nominal yang tertera pada struk pembelian',
          'nospbu.required' => 'Nomor SPBU wajib di isi'
      ];

      $valid = Validator::make($request->all(), $rule, $message);

      if ($valid->fails()) {
        return redirect()->back()
          ->withErrors($valid)
          ->withInput()
          ->with('error', 'Upload kedua foto struk');
      } else {

        if ($request->session()->has('img1') && $request->session()->has('img2')) {

          $bin_poin = 1; // one
          $receipt = Receipt::find($request->session()->get('id'));
          $receipt->nominal = $request->input('nominal');
          $receipt->namabank = $request->input('bank');
          $receipt->pembayaran = $request->input('payment');
          $receipt->nospbu = $request->input('nospbu');
          $receipt->nocc = $request->input('nocc');

          if ($request->input('payment') == 'kredit') {
              $bin = Bin::where('number', 'like', $request->input('nocc').'%')->first(); // two
              // if bin number exists
              if ($bin != null) {
                $receipt->cctype = $bin->type;
                // set point to bin point
                  // if the nominal above 100.000
                  if ($request->input('nominal') >= 100000) { // three
                    $bin_poin = $bin->poin;
                  } else {
                    $bin_poin = 1;
                  }
              }
          }

          $receipt->poin = round($request->input('nominal') / 100000 * $bin_poin, 2);

          if ($receipt->save()) {

            $request->session()->forget('id');
            $request->session()->forget('img1');
            $request->session()->forget('img2');
            return redirect('/struk-uploaded')
              ->with('message', 'Struk berhasil disimpan');
          } else {
            return redirect()->back()
              ->with('error', 'Gagal menyimpan. Silahkan ulangi');
          }

        } else {

          return redirect()->back()
            ->withInput()
            ->with('error', 'Upload kedua foto struk');

        }
      }

    }

    public function getUploaded()
    {
      return view('struk-uploaded');
    }

    // upload struk a
    public function upload_struk_a(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'photo' => 'mimes:jpg,jpeg,gif,png|image|max:5000'
        ]);

        if ($validator->fails()) {

          return response()->json($validator->errors());

        } else {

          if ($request->hasFile('photo')) {

            if ($request->session()->has('id')) {

                $file = $request->file('photo');
                $filename = 'id_' . $request->input('id') . '_' . mt_rand(1,9999) . '_struk_' . 'A';

                //Cloudder::upload($file, $filename, array(), array());

                $destinationPath = public_path('uploads/' . $filename . '.' . $file->getClientOriginalExtension());
                $img = Image::make($request->file('photo'));
                $img->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save($destinationPath);
                $img_url = url('/uploads'. '/'. $filename . '.' . $file->getClientOriginalExtension());

                $receipt = Receipt::find($request->session()->get('id'));
                $receipt->struk1 = $img_url;
                if ($receipt->save()) {
                  $request->session()->put('img1', $img_url);
                  return response($img_url, 200)->header('Content-Type', 'text/plain');
                }


              } else {

                $file = $request->file('photo');
                $filename = 'id_' . $request->input('id') . '_' . mt_rand(1,9999) . '_struk_' . 'A';

                $destinationPath = public_path('uploads/' . $filename . '.' . $file->getClientOriginalExtension());
                $img = Image::make($request->file('photo'));
                $img->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save($destinationPath);
                $img_url = url('/uploads'. '/'. $filename . '.' . $file->getClientOriginalExtension());

                $receipt = new Receipt;
                $receipt->user_id = $request->input('id');
                $receipt->struk1 = $img_url;
                if ($receipt->save()) {
                  $request->session()->put('id', $receipt->id);
                  $request->session()->put('img1', $img_url);
                  return response($img_url, 200)->header('Content-Type', 'text/plain');
                }

              }

          }

        }

    }


    // upload struk b
    public function upload_struk_b(Request $request)
    {

      $validator = Validator::make($request->all(), [
          'photo' => 'mimes:jpg,jpeg,gif,png|image|max:5000'
      ]);

      if ($validator->fails()) {

        return response()->json($validator->errors());

      } else {

        if ($request->hasFile('photo')) {

          if ($request->session()->has('id')) {

            $file = $request->file('photo');
            $filename = 'id_' . $request->input('id') . '_' . mt_rand(1,9999) . '_struk_' . 'B';

            $destinationPath = public_path('uploads/' . $filename . '.' . $file->getClientOriginalExtension());
            $img = Image::make($request->file('photo'));
            $img->resize(800, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($destinationPath);
            $img_url = url('/uploads'. '/'. $filename . '.' . $file->getClientOriginalExtension());

            $receipt = Receipt::find($request->session()->get('id'));
            $receipt->struk2 = $img_url;
            if ($receipt->save()) {
              $request->session()->put('img2', $img_url);
              return response($img_url, 200)->header('Content-Type', 'text/plain');
            }


          } else {

            $file = $request->file('photo');
            $filename = 'id_' . $request->input('id') . '_' . mt_rand(1,9999) . '_struk_' . 'B';

            $destinationPath = public_path('uploads/' . $filename . '.' . $file->getClientOriginalExtension());
            $img = Image::make($request->file('photo'));
            $img->resize(800, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($destinationPath);
            $img_url = url('/uploads'. '/'. $filename . '.' . $file->getClientOriginalExtension());

            $receipt = new Receipt;
            $receipt->user_id = $request->input('id');
            $receipt->struk2 = $img_url;
            if ($receipt->save()) {
              $request->session()->put('id', $receipt->id);
              $request->session()->put('img2', $img_url);
              return response($img_url, 200)->header('Content-Type', 'text/plain');
            }

          }

        }

      }

    }
}
