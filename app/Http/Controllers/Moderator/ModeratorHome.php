<?php

namespace App\Http\Controllers\Moderator;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use App\User;
use App\Receipt;
use App\Point;
use App\Bin;

use Validator;
use Excel;
use URL;
use Session;

class ModeratorHome extends Controller
{
    public function index()
    {

      $handle = Auth::user()->handle;
      $receipts = Receipt::where('namabank', $handle)->count();
      $receipts_waiting = Receipt::where('namabank', $handle)
        ->where('status', 0)->count();
      $receipts_approved = Receipt::where('namabank', $handle)->where('status', 1)->count();
      $receipts_declined = Receipt::where('namabank', $handle)->where('status', 2)->count();
      $admin_bni = User::where('handle', 'bni')->get();
      $admin_bri = User::where('handle', 'bri')->get();
      $admin_mandiri = User::where('handle', 'mandiri')->get();

      return view('moderator.home')
        ->with('receipts', $receipts)
        ->with('admin_bni', $admin_bni)
        ->with('admin_bri', $admin_bri)
        ->with('admin_mandiri', $admin_mandiri)
        ->with('receipts_waiting', $receipts_waiting)
        ->with('receipts_approved', $receipts_approved)
        ->with('receipts_declined', $receipts_declined);
    }

    public function check()
    {
      $handle = Auth::user()->handle;
      $keyword = null;
      $receipts = Receipt::where('namabank', $handle)
        ->where(function ($query) {
              $query->where('receipts.status', 0)
                    ->where('receipts.approvalcode', null);
          })
        ->orderBy('created_at', 'ASC')
        ->paginate(50);
      return view('moderator.check')
        ->with('status', 0)
        ->with('keyword', $keyword)
        ->with('receipts', $receipts);
    }

    // search
    public function checkSearch(Request $request)
    {
      $receipts = Receipt::where('users.name', 'like', '%'.$request->input('key') .'%')
        ->where(function ($query) {
              $handle = Auth::user()->handle;
              $query->where('receipts.status', 0)
                    ->where('receipts.approvalcode', null)
                    ->where('receipts.namabank', $handle);
          })
        ->join('users', 'users.id', 'receipts.user_id')
        ->select('receipts.*','users.id as uid')
        ->orderBy('created_at', 'ASC')
        ->paginate(50);
      return view('moderator.check')
        ->with('keyword', $request->input('key'))
        ->with('receipts', $receipts);
    }

    // halaman peserta
    public function participant(Request $request)
    {
        $users = User::orderBy('id', 'DESC')
          ->paginate(50);
      
        return view('moderator.participant')
          ->with('keyword', null)
          ->with('users', $users);
    }

    public function participantFind($id)
    {
        $user = User::find($id);
        $receipts = Receipt::where('user_id', $id)->paginate(20);
        return view('moderator.participant-detail')
          ->with('receipts', $receipts)
          ->with('user', $user);
    }

    public function participantSearch(Request $request)
    {
        $users = User::orderBy('id', 'DESC')
          ->where('name', 'like', '%'.$request->input('key') .'%')
          ->orWhere('email', 'like', '%'.$request->input('key') .'%')
          ->paginate(50);
      
        return view('moderator.participant')
          ->with('keyword', $request->input('key'))
          ->with('users', $users);
    }

    // edit receipt
    public function edit($id)
    {
      $receipt = Receipt::find($id);
      if ($receipt != null) {
        if ($receipt->approvalcode != null) {
          $related_receipts = Receipt::where('approvalcode', $receipt->approvalcode)
            ->where('id', '<>', $id)
            ->get();
        } else {
          $related_receipts = null;
        }
      } else {
        return redirect('moderator/check')
          ->with('error', 'Struk tidak ditemukan');
      }
      return view('moderator.edit')
        ->with('receipt', $receipt)
        ->with('related_receipts', $related_receipts);
    }

    // update Data
    public function update(Request $request)
    {
      $id = $request->input('id');
      $url = Session::get('previous_url');

      $valid = Validator::make($request->all(), [
        'nominal' => 'required|numeric',
        'bensin'  => 'required',
        'approvalcode'  => 'required|numeric',
        'lastfourdigits'  => 'required|numeric',
        'nospbu'  => 'max:20',
      ]);

      if ($valid->fails()) {
        return redirect()->back()
          ->withErrors($valid)
          ->withinput();
      } else {

        $receipt = Receipt::find($id);
        $receipt->nominal = $request->input('nominal');

        // careful
        $bin_poin = 1;
        $bin = Bin::where('number', 'like', $receipt->nocc.'%')->first(); // two

        // if bin number exists
        if ($bin != null) {
            // if the nominal above 100.000
            if ($request->input('nominal') >= 100000) { // three
              $bin_poin = $bin->poin;
            } else {
              $bin_poin = 1;
            }
        } else {
            $bin_poin = 1;
        }

        $receipt->poin = round($request->input('nominal') / 100000 * $bin_poin, 2);

        $receipt->bensin = $request->input('bensin');
        $receipt->approvalcode = ( $request->input('approvalcode') == '' ? null : $request->input('approvalcode') );
        $receipt->lastfourdigits = $request->input('lastfourdigits');
        $receipt->nospbu = $request->input('nospbu');
        
        if ($request->input('nominal') < 800000) {
          $receipt->status = 1;
        } else {
          $receipt->status = 0;
        }

        if ($receipt->save()) {
          return redirect()->back()
            ->with('message', 'Data struk berhasil di rubah');
        } else {
          return redirect()
            ->back()
            ->withErrors($valid)
            ->withInput();
        }

      }
    }


    // Approval method
    public function approval_page()
    {
      $handle = Auth::user()->handle;
      $receipts = Receipt::where('namabank', $handle)
        ->where('status', 0)
        ->where('approvalcode', '<>', '')
        ->where('nominal', '<', 800000)
        ->oldest()
        ->paginate(50);
      $waiting = Receipt::where('namabank', $handle)
        ->where('status', 0)
        ->where('approvalcode', '<>', '')
        ->where('nominal', '<', 800000)
        ->count();
      $approved = Receipt::where('namabank', $handle)
          ->where('status', 1)->count();
      $declined = Receipt::where('namabank', $handle)
            ->where('status', 2)->count();
      $eight = Receipt::where('nominal', '>=', 800000)
            ->where('approvalcode', '<>', '')
            ->where('status', 0)->count();
      return view('moderator.approval')
        ->with('status', 0)
        ->with('keyword', null)
        ->with('waiting', $waiting)
        ->with('approved', $approved)
        ->with('declined', $declined)
        ->with('eight', $eight)
        ->with('receipts', $receipts);
    }

    public function approval_pageSearch(Request $request)
    {

      $handle = Auth::user()->handle;
      $waiting = Receipt::where('receipts.namabank', $handle)
        ->where('receipts.status', 0)
        ->where('receipts.approvalcode', '<>', '')
        ->where('nominal', '<', 800000)
        ->join('users', 'users.id', 'receipts.user_id')
        ->where('users.name', 'like', '%'.$request->input('key') .'%')
        ->select('receipts.*','users.id as uid')
        ->count();
      $approved = Receipt::where('namabank', $handle)
          ->where('status', 1)->count();
      $declined = Receipt::where('namabank', $handle)
            ->where('status', 2)->count();
      $eight = Receipt::where('nominal', '>=', 800000)
            ->where('approvalcode', '<>', '')
            ->where('status', 0)->count();
      $receipts = Receipt::where('receipts.namabank', $handle)
        ->where('receipts.status', 0)
        ->where('receipts.approvalcode', '<>', '')
        ->join('users', 'users.id', 'receipts.user_id')
        ->where('users.name', 'like', '%'.$request->input('key') .'%')
        ->select('receipts.*','users.id as uid')
        ->paginate(50);
      return view('moderator.approval')
        ->with('keyword', $request->input('key'))
        ->with('status', 0)
        ->with('waiting', $waiting)
        ->with('approved', $approved)
        ->with('declined', $declined)
        ->with('eight', $eight)
        ->with('receipts', $receipts);
    }

    // Approval method
    public function approval_above_eight()
    {
      $handle = Auth::user()->handle;
      $receipts = Receipt::where('status', 0)
        ->where('approvalcode', '<>', '')
        ->where('nominal', '>=', 800000)
        ->oldest()
        ->paginate(50);
      $waiting = Receipt::where('namabank', $handle)
        ->where('status', 0)
        ->where('approvalcode', '<>', '')
        ->where('nominal', '<', 800000)
        ->count();
      $approved = Receipt::where('namabank', $handle)
          ->where('status', 1)->count();
      $declined = Receipt::where('namabank', $handle)
            ->where('status', 2)->count();
      $eight = Receipt::where('nominal', '>=', 800000)
            ->where('approvalcode', '<>', '')
            ->where('status', 0)->count();
      return view('moderator.above-eight')
        ->with('status', 0)
        ->with('keyword', null)
        ->with('waiting', $waiting)
        ->with('approved', $approved)
        ->with('declined', $declined)
        ->with('eight', $eight)
        ->with('receipts', $receipts);
    }

    public function approval_above_eightSearch(Request $request)
    {

      $handle = Auth::user()->handle;
      $waiting = Receipt::where('receipts.namabank', $handle)
        ->where('receipts.status', 0)
        ->where('nominal', '<', 800000)
        ->where('receipts.approvalcode', '<>', '')
        ->count();
      $approved = Receipt::where('namabank', $handle)
          ->where('status', 1)->count();
      $declined = Receipt::where('namabank', $handle)
            ->where('status', 2)->count();
      $eight = Receipt::where('nominal', '>=', 800000)
            ->where('approvalcode', '<>', '')
            ->join('users', 'users.id', 'receipts.user_id')
            ->where('users.name', 'like', '%'.$request->input('key') .'%')
            ->select('receipts.*','users.id as uid')
            ->where('status', 0)->count();
      $receipts = Receipt::where('receipts.namabank', $handle)
        ->where('receipts.status', 0)
        ->where('receipts.approvalcode', '<>', '')
        ->join('users', 'users.id', 'receipts.user_id')
        ->where('users.name', 'like', '%'.$request->input('key') .'%')
        ->select('receipts.*','users.id as uid')
        ->paginate(50);
      return view('moderator.approval')
        ->with('keyword', $request->input('key'))
        ->with('status', 0)
        ->with('waiting', $waiting)
        ->with('approved', $approved)
        ->with('declined', $declined)
        ->with('eight', $eight)
        ->with('receipts', $receipts);
    }

    

    public function approved()
    {
      $handle = Auth::user()->handle;
      $receipts = Receipt::where('namabank', $handle)
        ->where('status', 1)
        ->where('approvalcode', '<>', '')
        ->oldest()
        ->paginate(50);
      $waiting = Receipt::where('namabank', $handle)
        ->where('status', 0)
        ->where('approvalcode', '<>', '')
        ->where('nominal', '<', 800000)
        ->count();
      $approved = Receipt::where('namabank', $handle)
          ->where('approvalcode', '<>', '')
          ->where('status', 1)->count();
      $declined = Receipt::where('namabank', $handle)
            ->where('status', 2)->count();
      $eight = Receipt::where('nominal', '>=', 800000)
            ->where('approvalcode', '<>', '')
            ->where('status', 0)->count();
      return view('moderator.approved')
        ->with('status', 1)
        ->with('keyword', null)
        ->with('receipts', $receipts)
        ->with('waiting', $waiting)
        ->with('approved', $approved)
        ->with('eight', $eight)
        ->with('declined', $declined);
    }

    public function approvedSearch(Request $request)
    {

      $handle = Auth::user()->handle;
      $waiting = Receipt::where('namabank', $handle)
        ->where('status', 0)
        ->where('approvalcode', '<>', '')
        ->where('nominal', '<', 800000)
        ->count();
      $approved = Receipt::where('receipts.namabank', $handle)
          ->where('receipts.status', 1)
          ->join('users', 'users.id', 'receipts.user_id')
          ->where('users.name', 'like', '%'.$request->input('key') .'%')
          ->select('receipts.*','users.id as uid')
          ->count();
      $declined = Receipt::where('namabank', $handle)
            ->where('status', 2)->count();
      $eight = Receipt::where('nominal', '>=', 800000)
            ->where('approvalcode', '<>', '')
            ->where('status', 0)->count();
      $receipts = Receipt::where('receipts.namabank', $handle)
        ->where('receipts.status', 1)
        ->join('users', 'users.id', 'receipts.user_id')
        ->where('users.name', 'like', '%'.$request->input('key') .'%')
        ->select('receipts.*','users.id as uid')
        ->paginate(50);
      return view('moderator.approved')
        ->with('keyword', $request->input('key'))
        ->with('status', 1)
        ->with('waiting', $waiting)
        ->with('approved', $approved)
        ->with('declined', $declined)
        ->with('eight', $eight)
        ->with('receipts', $receipts);
    }

    public function declined()
    {
      $handle = Auth::user()->handle;
      $receipts = Receipt::where('namabank', $handle)
        ->where('status', 2)
        ->orderBy('created_at', 'ASC')
        ->paginate(50);
      $waiting = Receipt::where('namabank', $handle)
        ->where('status', 0)
        ->where('bensin', '<>', '')
        ->where('nominal', '<', 800000)
        ->count();
      $approved = Receipt::where('namabank', $handle)
          ->where('status', 1)->count();
      $declined = Receipt::where('namabank', $handle)
            ->where('status', 2)->count();
      $eight = Receipt::where('nominal', '>=', 800000)
            ->where('approvalcode', '<>', '')
            ->where('status', 0)->count();
      return view('moderator.declined')
        ->with('status', 2)
        ->with('receipts', $receipts)
        ->with('waiting', $waiting)
        ->with('approved', $approved)
        ->with('eight', $eight)
        ->with('declined', $declined);
    }



    /*
    * Data
    *
    */
    public function data()
    {
      return view('moderator.data.view');
    }


    public function dataPost(Request $request)
    {
      $users = Receipt::where('receipts.namabank', $request->input('bank'))
        //->where('receipts.status', '<>', 2)
        ->whereBetween('receipts.created_at', [$request->input('from'), $request->input('to')])
        ->leftJoin('users', 'users.id', '=', 'receipts.user_id')
        ->select('receipts.id','receipts.status','users.name', 'users.email', 'users.phone', 'users.ktp', 'receipts.struk1', 'receipts.struk2','receipts.nominal', 'receipts.namabank','receipts.pembayaran','receipts.approvalcode','receipts.nocc as enam_digit_pertama','receipts.lastfourdigits as empat_digit_terakhir', 'receipts.cctype as jenis_kartu','receipts.poin','receipts.nospbu','receipts.bensin as jenis_bensin','receipts.created_at')
        ->oldest()
        ->get();
      Excel::create('struk'. time(), function($excel) use($users) {
          $excel->sheet('Sheet 1', function($sheet) use($users) {
              $sheet->fromArray($users);
          });
      })->download('csv');
    }




    // approve receipts
    public function approve(Request $request)
    {
      $id = $request->input('id');
      $receipt = Receipt::find($id);
      $receipt->status = 1;
      if ($receipt->save()) {

        $bank = Auth::user()->handle;
        $struk = Receipt::find($receipt->id);
        $point = Point::where('user_id', $struk->user_id)->first();

        if ($bank == 'mandiri') {
          $point->mandiri_poin = $point->mandiri_poin + $struk->poin;
          $point->mandiri_chance = floor($point->mandiri_poin / 5);
        } elseif ($bank == 'bni') {
          $point->bni_poin = $point->bni_poin + $struk->poin;
          $point->bni_chance = floor($point->bni_poin / 5);
        } else {
          $point->bri_poin = $point->bri_poin + $struk->poin;
          $point->bri_chance = floor($point->bri_poin / 5);
        }

        $point->save();

        return redirect()->back()
          ->with('message', 'Struk berhasil disetujui');
      } else {
        return redirect()->back()
          ->with('error','Struk gagal di setujui, silahkan coba lagi');
      }
    }

    // decline receipt
    public function decline(Request $request)
    {
      $id = $request->input('id');
      $receipt = Receipt::find($id);
      $receipt->status = 2;
      if ($receipt->save()) {

        // $bank = Auth::user()->handle;
        // $struk = Receipt::find($receipt->id);
        // $point = Point::where('user_id', $struk->user_id)->first();

        // if ($bank == 'mandiri') {
        //   $point->mandiri_poin = $point->mandiri_poin - $struk->poin;
        //   $point->mandiri_chance = floor($point->mandiri_poin / 5);
        // } elseif ($bank == 'bni') {
        //   $point->bni_poin = $point->bni_poin - $struk->poin;
        //   $point->bni_chance = floor($point->bni_poin / 5);
        // } else {
        //   $point->bri_poin = $point->bri_poin - $struk->poin;
        //   $point->bri_chance = floor($point->bri_poin / 5);
        // }

        // $point->save();

        return redirect()->back()
          ->with('message', 'Struk berhasil ditolak');
      } else {
        return redirect()->back()
          ->with('error','Struk gagal di tolak, silahkan coba lagi');
      }
    }

    public function reset(Request $request)
    {
      $id = $request->input('id');
      $receipt = Receipt::find($id);
      $receipt->status = 0;
      if ($receipt->save()) {
        return redirect()->back()
          ->with('message', 'Struk berhasil direset');
      } else {
        return redirect()->back()
          ->with('error','Struk gagal direset, silahkan coba lagi');
      }
    }

    // deleting receipt
    public function delete(Request $request)
    {
      $receipt = Receipt::find($request->input('id'));
      if ($receipt->delete()) {
        return redirect()->back()
          ->with('message', 'Data berhasil dihapus');
      } else {
        return redirect()->back()
          ->with('error', 'Gagal menghapus, silahkan coba lagi');
      }
    }

    // mass approve datas
    // public function approveAll()
    // {

    //   $receipts = Receipt::join('receipts_approved', 'receipts_approved.id', 'receipts.id')
    //       ->where('status', '<>', 1)
    //       ->update(['status' => 1]);

    //   if ($receipts == true) {
    //     return redirect('moderator/check');
    //   } else {
    //     return 'oops';
    //   }

    // }

    // export to csv
    public function export($fileType, $page)
    {
      $users = Receipt::where('receipts.namabank', Auth::user()->handle)
        ->where('receipts.status', $page)
        ->where('receipts.approvalcode', '<>', '')
        ->leftJoin('users', 'users.id', '=', 'receipts.user_id')
        ->select('users.name', 'users.email', 'users.phone', 'users.ktp', 'receipts.struk1', 'receipts.struk2','receipts.nominal', 'receipts.namabank','receipts.pembayaran','receipts.approvalcode','receipts.nocc as enam_digit_pertama','receipts.lastfourdigits as empat_digit_terakhir', 'receipts.cctype as jenis_kartu','receipts.poin','receipts.nospbu','receipts.bensin as jenis_bensin','receipts.created_at')
        ->oldest()
        ->get();
      Excel::create('Struk', function($excel) use($users) {
          $excel->sheet('Sheet 1', function($sheet) use($users) {
              $sheet->fromArray($users);
          });
      })->download($fileType);
    }

    public function exportEight()
    {
      $users = Receipt::where('receipts.status', 0)
        ->where('receipts.approvalcode', '<>', '')
        ->where('receipts.nominal', '>=', 800000)
        ->leftJoin('users', 'users.id', '=', 'receipts.user_id')
        ->select('users.name', 'users.email', 'users.phone', 'users.ktp', 'receipts.struk1', 'receipts.struk2','receipts.nominal', 'receipts.namabank','receipts.pembayaran','receipts.approvalcode','receipts.nocc as enam_digit_pertama','receipts.lastfourdigits as empat_digit_terakhir', 'receipts.cctype as jenis_kartu','receipts.poin','receipts.nospbu','receipts.bensin as jenis_bensin','receipts.created_at')
        ->oldest()
        ->get();
      Excel::create('Struk', function($excel) use($users) {
          $excel->sheet('Sheet 1', function($sheet) use($users) {
              $sheet->fromArray($users);
          });
      })->download('csv');
    }

    public function exportUsers($fileType)
    {
      $users = User::where('role', 0)
        ->join('points', 'users.id', '=', 'points.user_id')
        ->where('block', 0)
        ->select('users.*', 'points.mandiri_poin', 'points.mandiri_chance', 'bni_poin', 'bni_chance', 'bri_poin', 'bri_chance')
        ->oldest()
        ->get();
      Excel::create('Users', function($excel) use($users) {
          $excel->sheet('Sheet 1', function($sheet) use($users) {
              $sheet->fromArray($users);
          });
      })->download($fileType);
    }

}
