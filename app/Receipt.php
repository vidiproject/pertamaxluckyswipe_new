<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Receipt extends Model
{
    use SoftDeletes;

    protected $table = 'receipts';
    protected $fillable = ['struk1', 'struk2', 'nospbu', 'nominal', 'namabank', 'pembayaran', 'approvalcode', 'bensin', 'nocc', 'poin', 'status', 'lastfourdigits'];
    protected $dates = ['deleted_at'];

    public function user()
  	{
  		return $this->belongsTo('App\User');
  	}

}
