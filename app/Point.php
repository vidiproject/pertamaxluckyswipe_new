<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Point extends Model
{
    protected $table = 'points';
    protected $fillable = ['user_id','mandiri_poin','mandiri_chance', 'bni_poin', 'bni_chance', 'bri_poin', 'bri_chance'];

    // user
    public function user()
  	{
  		return $this->belongsTo('App\User');
  	}
}
