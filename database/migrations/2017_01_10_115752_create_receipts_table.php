<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipts', function (Blueprint $table) {
            $table->increments('id');

            $table->bigInteger('user_id');
            $table->string('struk1', 255);
            $table->string('struk2', 255);
            $table->string('nominal', 100); // nominal pembelian
            $table->string('namabank', 10); // "bni", 'bri', 'mandiri'
            $table->string('pembayaran', 10); // "debit","kredit"
            $table->string('nocc')->nullable(); // no credit card
            $table->tinyInteger('status')->default(0); // '1', '2', '3'
            $table->string('poin');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receipts');
    }
}
