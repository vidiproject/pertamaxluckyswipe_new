<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');

            $table->string('phone', 20);
            $table->string('ktp', 20);
            $table->ipAddress('ip');
            $table->boolean('block')->default(0); // "0", "1"
            $table->tinyInteger('role')->default(0); // "1 = admin", "2 = moderator", "0 = customer"
            $table->string('handle', 10)->nullable(); // "null", "bni", "bri", "mandiri"

            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
