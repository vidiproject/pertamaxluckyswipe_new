@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4 text-center">

          <p><img class="img-responsive img-center" src="{{ url('img/image-lucky-swipe.png') }}" alt="Logo Lucky Swipe"></p>
          <h4 class="blue-title"><span style="font-size: 32px; font-weight: 600">Bayar dengan Kartu,</span> <span style="font-weight: 600"><br>Kini Bebas Biaya Tambahan</span></h4>

            <div class="text-right">
                <a href="{{ url('home/edit') }}">
                    Edit Profil
                </a>
            </div>

            @if(Session::has('message'))
                <span class="text-success">{{ Session::get('message') }}</span>
            @elseif(Session::has('error'))
                <span class="text-danger">{{ Session::get('error') }}</span>
            @endif

            <h3><strong>Halo, {{ Auth::user()->name }}</strong></h3>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="mandiri">

                <div class="carousel quote-carousel slide" data-ride="carousel" data-interval="false" id="quote-carousel">
                    <!-- Bottom Carousel Indicators -->
                    <!-- <ol class="carousel-indicators">
                        <li data-target="#quote-carousel" data-slide-to="0" class="active"><img class="img-responsive " src="https://s3.amazonaws.com/uifaces/faces/twitter/brad_frost/128.jpg" alt="">
                        </li>
                        <li data-target="#quote-carousel" data-slide-to="1"><img class="img-responsive" src="https://s3.amazonaws.com/uifaces/faces/twitter/rssems/128.jpg" alt="">
                        </li>
                        <li data-target="#quote-carousel" data-slide-to="2"><img class="img-responsive" src="https://s3.amazonaws.com/uifaces/faces/twitter/adellecharles/128.jpg" alt="">
                        </li>
                    </ol> -->

                    <!-- Carousel Slides / Quotes -->
                    <div class="carousel-inner text-center">
                        <!-- Quote 1 -->
                        <div class="item active">
                            <blockquote>
                                <div class="row">
                                    <div class="col-sm-8 col-sm-offset-2" style="margin: 0 auto;">
                                          <span class="flying-info">Total poin kamu saat ini</span>
                                          <h3>{{ floor($poin_mandiri) }}</h3>
                                    </div>
                                </div>
                            </blockquote>
                        </div>
                        <!-- Quote 2 -->
                        <div class="item">
                            <blockquote>
                                <div class="row">
                                    <div class="col-sm-8 col-sm-offset-2" style="margin: 0 auto;">
                                        <span class="flying-info">Poin yang disetujui</span>
                                        <h3>{{ floor($poin_mandiri1) }}</h3>
                                    </div>
                                </div>
                            </blockquote>
                        </div>
                        <!-- Quote 3 -->
                        <div class="item">
                            <blockquote>
                                <div class="row">
                                    <div class="col-sm-8 col-sm-offset-2" style="margin: 0 auto;">
                                        <span class="flying-info">Poin yang belum disetujui</span>
                                        <h3>{{ floor($poin_mandiri3) }}</h3>
                                    </div>
                                </div>
                            </blockquote>
                        </div>
                    </div>

                    <!-- Carousel Buttons Next/Prev -->
                    <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
                    <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>

              </div>
              <div role="tabpanel" class="tab-pane" id="bni">

                <div class="carousel quote-carousel slide" data-ride="carousel" data-interval="false" id="quote-carousel2">
                    <!-- Bottom Carousel Indicators -->
                    <!-- <ol class="carousel-indicators">
                        <li data-target="#quote-carousel2" data-slide-to="0" class="active"><img class="img-responsive " src="https://s3.amazonaws.com/uifaces/faces/twitter/brad_frost/128.jpg" alt="">
                        </li>
                        <li data-target="#quote-carousel2" data-slide-to="1"><img class="img-responsive" src="https://s3.amazonaws.com/uifaces/faces/twitter/rssems/128.jpg" alt="">
                        </li>
                        <li data-target="#quote-carousel2" data-slide-to="2"><img class="img-responsive" src="https://s3.amazonaws.com/uifaces/faces/twitter/adellecharles/128.jpg" alt="">
                        </li>
                    </ol> -->

                    <!-- Carousel Slides / Quotes -->
                    <div class="carousel-inner text-center">
                        <!-- Quote 1 -->
                        <div class="item active">
                            <blockquote>
                                <div class="row">
                                    <div class="col-sm-8 col-sm-offset-2" style="margin: 0 auto;">
                                        <span class="flying-info">Total poin kamu saat ini</span>
                                        <h3>{{ floor($poin_bni) }}</h3>
                                    </div>
                                </div>
                            </blockquote>
                        </div>
                        <!-- Quote 2 -->
                        <div class="item">
                            <blockquote>
                                <div class="row">
                                    <div class="col-sm-8 col-sm-offset-2" style="margin: 0 auto;">
                                        <span class="flying-info">Poin yang disetujui</span>
                                        <h3>{{ floor($poin_bni1) }}</h3>
                                    </div>
                                </div>
                            </blockquote>
                        </div>
                        <!-- Quote 3 -->
                        <div class="item">
                            <blockquote>
                                <div class="row">
                                    <div class="col-sm-8 col-sm-offset-2" style="margin: 0 auto;">
                                        <span class="flying-info">Poin yang belum disetujui</span>
                                        <h3>{{ floor($poin_bni3) }}</h3>
                                    </div>
                                </div>
                            </blockquote>
                        </div>
                    </div>

                    <!-- Carousel Buttons Next/Prev -->
                    <a data-slide="prev" href="#quote-carousel2" class="left carousel-control"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
                    <a data-slide="next" href="#quote-carousel2" class="right carousel-control"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>

              </div>
              <div role="tabpanel" class="tab-pane" id="bri">

                <div class="carousel quote-carousel slide" data-ride="carousel" data-interval="false" id="quote-carousel3">
                    <!-- Bottom Carousel Indicators -->
                    <!-- <ol class="carousel-indicators">
                        <li data-target="#quote-carousel2" data-slide-to="0" class="active"><img class="img-responsive " src="https://s3.amazonaws.com/uifaces/faces/twitter/brad_frost/128.jpg" alt="">
                        </li>
                        <li data-target="#quote-carousel2" data-slide-to="1"><img class="img-responsive" src="https://s3.amazonaws.com/uifaces/faces/twitter/rssems/128.jpg" alt="">
                        </li>
                        <li data-target="#quote-carousel2" data-slide-to="2"><img class="img-responsive" src="https://s3.amazonaws.com/uifaces/faces/twitter/adellecharles/128.jpg" alt="">
                        </li>
                    </ol> -->

                    <!-- Carousel Slides / Quotes -->
                    <div class="carousel-inner text-center">
                        <!-- Quote 1 -->
                        <div class="item active">
                            <blockquote>
                                <div class="row">
                                    <div class="col-sm-8 col-sm-offset-2" style="margin: 0 auto;">
                                        <span class="flying-info">Total poin kamu saat ini</span>
                                        <h3>{{ floor($poin_bri) }}</h3>
                                    </div>
                                </div>
                            </blockquote>
                        </div>
                        <!-- Quote 2 -->
                        <div class="item">
                            <blockquote>
                                <div class="row">
                                    <div class="col-sm-8 col-sm-offset-2" style="margin: 0 auto;">
                                        <span class="flying-info">Poin yang disetujui</span>
                                        <h3>{{ floor($poin_bri1) }}</h3>
                                    </div>
                                </div>
                            </blockquote>
                        </div>
                        <!-- Quote 3 -->
                        <div class="item">
                            <blockquote>
                                <div class="row">
                                    <div class="col-sm-8 col-sm-offset-2" style="margin: 0 auto;">
                                        <span class="flying-info">Poin yang belum disetujui</span>
                                        <h3>{{ floor($poin_bri3) }}</h3>
                                    </div>
                                </div>
                            </blockquote>
                        </div>
                    </div>

                    <!-- Carousel Buttons Next/Prev -->
                    <a data-slide="prev" href="#quote-carousel3" class="left carousel-control"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
                    <a data-slide="next" href="#quote-carousel3" class="right carousel-control"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>

              </div>
            </div>


            <div>
              <ul class="nav nav-tabs tabs-img" role="tablist">
                <li role="presentation" class="active"><a href="#mandiri" aria-controls="mandiri" role="tab" data-toggle="tab">
                  <img src="{{ url('img/mandiri.jpg') }}" class="img-responsive" alt="">
                </a></li>
                <li role="presentation"><a href="#bni" aria-controls="bni" role="tab" data-toggle="tab">
                  <img src="{{ url('img/bni.jpg') }}" class="img-responsive" alt="">
                </a></li>
                <li role="presentation"><a href="#bri" aria-controls="bri" role="tab" data-toggle="tab">
                  <img src="{{ url('img/bri.jpg') }}" class="img-responsive" alt="">
                </a></li>
              </ul>
            </div>

            <p>Tingkatkan terus poin untuk menangkan hadiah menarik dari Pertamina Fuel Lucky Swipe. Poin ini bersifat sementara dan akan ada verifikasi dari bank.</p>

            <br>

            <a href="{{ url('home/upload-struk') }}" class="btn btn-primary triangle">Upload Struk</a>

            <br><br>

        </div>
    </div>
</div>
@include('layouts.footer')
@endsection
