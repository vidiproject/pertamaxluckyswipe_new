@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2">
      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active triangle"><a href="{{ url('login') }}" aria-controls="login" role="tab">Masuk</a></li>
        <li role="presentation" class="triangle-white"><a href="{{ url('register') }}" aria-controls="register" role="tab">Daftar</a></li>
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="login">
          <form role="form" method="POST" action="{{ url('/login') }}">
              {{ csrf_field() }}

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email">Email</label>
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password">Password</label>
                    <input id="password" type="password" class="form-control" name="password">
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

              <!-- <p>
                  <input type="checkbox" class="filled-in" id="filled-in-box" name="remember">
                  <label for="filled-in-box">Remember Me</label>
              </p> -->
              <div class="form-group">

                      <a href="{{ url('/password/reset') }}">
                          Forgot Your Password?
                      </a>
                      <button type="submit" class="btn btn-primary triangle pull-right">
                          Lanjutkan
                      </button>

              </div>
          </form>
        </div>
        <div role="tabpanel" class="tab-pane" id="register">
          <form role="form" method="POST" action="{{ url('/register') }}">
              {{ csrf_field() }}

              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                  <label for="name" class="control-label">Nama</label>
                  <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">

                  @if ($errors->has('name'))
                      <span class="help-block">
                          <strong>{{ $errors->first('name') }}</strong>
                      </span>
                  @endif
              </div>

              <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                  <label for="email" class="control-label">Email</label>
                  <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                  @if ($errors->has('email'))
                      <span class="help-block">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                  @endif
              </div>

              <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                  <label for="phone" class="control-label">No Hp</label>
                  <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}">
                  @if ($errors->has('phone'))
                      <span class="help-block">
                          <strong>{{ $errors->first('phone') }}</strong>
                      </span>
                  @endif
              </div>

              <div class="form-group{{ $errors->has('ktp') ? ' has-error' : '' }}">
                  <label for="ktp" class="control-label">No KTP</label>
                  <input id="ktp" type="text" class="form-control" name="ktp" value="{{ old('ktp') }}">

                  @if ($errors->has('ktp'))
                      <span class="help-block">
                          <strong>{{ $errors->first('ktp') }}</strong>
                      </span>
                  @endif
              </div>

              <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                  <label for="password" class="control-label">Password</label>
                  <input id="password" type="password" class="form-control" name="password">

                  @if ($errors->has('password'))
                      <span class="help-block">
                          <strong>{{ $errors->first('password') }}</strong>
                      </span>
                  @endif
              </div>

              <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                  <label for="password-confirm" class="control-label">Confirm Password</label>
                  <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                  @if ($errors->has('password_confirmation'))
                      <span class="help-block">
                          <strong>{{ $errors->first('password_confirmation') }}</strong>
                      </span>
                  @endif
              </div>

              <div class="form-group">
                  <button type="submit" class="btn btn-primary triangle pull-right">
                      Lanjutkan
                  </button>
              </div>
          </form>
          <br />
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
