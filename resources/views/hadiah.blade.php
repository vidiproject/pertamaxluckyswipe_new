@extends('layouts.app')
@section('content')

<div class="container">
  <div class="row">
    <div class="col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4 text-center">

      <p><img class="img-responsive img-center" src="{{ url('img/image-lucky-swipe.png') }}" alt="Logo Lucky Swipe"></p>
      <h4 class="blue-title"><span style="font-size: 32px; font-weight: 600">Bayar dengan Kartu,</span> <span style="font-weight: 600"><br>Kini Bebas Biaya Tambahan</span></h4>

      <br>

      <p>Menangkan hadiah spesial dari setiap pembelian Pertamax Series, Dex Series, Pertalite</p>

      <div class="">
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active triangle"><a href="#priode1" aria-controls="priode1" role="tab" data-toggle="tab">Periode 1</a></li>
          <li role="presentation" class="triangle"><a href="#priode2" aria-controls="priode2" role="tab" data-toggle="tab">Periode 2</a></li>
        </ul>

        <br>

        <!-- Tab panes -->
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="priode1">
            <img src="{{ url('img/periode-1.png') }}" class="img-responsive" alt="" />
            <br>
            <p>Periode 1 dari 30 Januari 2017 s/d 30 Juni 2017</p>
          </div>
          <div role="tabpanel" class="tab-pane" id="priode2">
            <img src="{{ url('img/periode-2.png') }}" class="img-responsive" alt="" />
            <br>
            <p>Periode 2 dari 1 Juli 2017 s/d 30 November 2017</p>
          </div>
        </div>
      </div>

      <br><br>

    </div>
  </div>
</div>
@include('layouts.footer')
@stop
