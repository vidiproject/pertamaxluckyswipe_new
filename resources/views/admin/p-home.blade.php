@extends('layouts.admin')
@section('content')

<div class="container">
  <h3>Statistics</h3>
  <div class="row">
    <div class="col-md-4">
      <div class="panel panel-default">
        <div class="panel-heading">Total Registrasi</div>
        <div class="panel-body text-center">
          <h4>{{ $total_users }}</h4>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="panel panel-default">
        <div class="panel-heading">Active User</div>
        <div class="panel-body text-center">
          <h4>{{ $unique_users }}</h4>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="panel panel-default">
        <div class="panel-heading">Submission</div>
        <div class="panel-body text-center">
          <h4>{{ $submision }}</h4>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="panel panel-primary">
        <div class="panel-heading">Jenis Bensin</div>
        <div class="panel-body">
          <ul class="list-group">
            <li class="list-group-item">Pertamax <span class="badge">{{ $pertamax }}</span></li>
            <li class="list-group-item">Pertamax Turbo <span class="badge">{{ $pertamax_turbo }}</span></li>
            <li class="list-group-item">Pertamina Dex <span class="badge">{{ $pertamina_dex }}</span></li>
            <li class="list-group-item">Pertalite <span class="badge">{{ $pertalite }}</span></li>
            <li class="list-group-item">Dexlite <span class="badge">{{ $dexlite }}</span></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">Submision per Wilayah</div>
        <div class="panel-body">
          <table class="table table-bordered table-striped table-responsive">
            <tr>
              <th>#</th>
              <th>No Spbu</th>
              <th>Wilayah</th>
              <th>Total</th>
            </tr>
            <?php $i = 1; $total = 0; ?>
            @foreach($daerah as $item)
            <?php $total += $item->total; ?>
            <tr>
              <td>{{ $i }}</td>
              <td>{{ ( $item->nospbu == null ? 'Others' : $item->nospbu ) }}</td>
              <td>{{ ( $item->provinsi == null ? 'Others' : $item->provinsi ) }}</td>
              <td>{{ $item->total }}</td>
            </tr>
            <?php $i++; ?>
            @endforeach
            <tr>
              <td>Total</td>
              <td></td>
              <td></td>
              <td><b>{{ $total }}</b></td>
            </tr>
          </table>
          {{ $daerah->links() }}
        </div>
      </div>
    </div>
  </div>

</div>

@stop
