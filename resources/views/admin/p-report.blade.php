@extends('layouts.admin')
@section('content')

<div class="container">
  <h3>Total Pembelian</h3>
  <div class="row">
    <div class="col-md-4">
      <div class="panel panel-default">
        <div class="panel-heading">Mandiri</div>
        <div class="panel-body">
          <ul class="list-group">
            <li class="list-group-item">Total Pembelian <span class="badge">Rp. {{ number_format($total_mandiri, 0, '', '.') }}</span></li>
            <li class="list-group-item">Debit <span class="badge">Rp. {{ number_format($total_mandiri_debit, 0, '', '.') }}</span></li>

            <li class="list-group-item" style="background-color: #fafafa;">Approve <span class="badge" style="background-color: steelblue">Rp. {{ number_format($total_mandiri_debit_approve, 0, '', '.') }}</span></li>
            <li class="list-group-item" style="background-color: #fafafa;">Not Approve <span class="badge" style="background-color: indianred;">Rp. {{ number_format($total_mandiri_debit_inapprove, 0, '', '.') }}</span></li>

            <li class="list-group-item">Kredit <span class="badge">Rp. {{ number_format($total_mandiri_kredit, 0, '', '.') }}</span></li>

            <li class="list-group-item" style="background-color: #fafafa;">Approve <span class="badge" style="background-color: steelblue">Rp. {{ number_format($total_mandiri_kredit_approve, 0, '', '.') }}</span></li>
            <li class="list-group-item" style="background-color: #fafafa;">Not Approve <span class="badge" style="background-color: indianred;">Rp. {{ number_format($total_mandiri_kredit_inapprove, 0, '', '.') }}</span></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="panel panel-default">
        <div class="panel-heading">BNI</div>
        <div class="panel-body">
          <ul class="list-group">
            <li class="list-group-item">Total Pembelian <span class="badge">Rp. {{ number_format($total_bni, 0, '', '.') }}</span></li>
            <li class="list-group-item">Debit <span class="badge">Rp. {{ number_format($total_bni_debit, 0, '', '.') }}</span></li>

            <li class="list-group-item" style="background-color: #fafafa;">Approve <span class="badge" style="background-color: steelblue">Rp. {{ number_format($total_bni_debit_approve, 0, '', '.') }}</span></li>
            <li class="list-group-item" style="background-color: #fafafa;">Not Approve <span class="badge" style="background-color: indianred;">Rp. {{ number_format($total_bni_debit_inapprove, 0, '', '.') }}</span></li>

            <li class="list-group-item">Kredit <span class="badge">Rp. {{ number_format($total_bni_kredit, 0, '', '.') }}</span></li>

            <li class="list-group-item" style="background-color: #fafafa;">Approve <span class="badge" style="background-color: steelblue">Rp. {{ number_format($total_bni_kredit_approve, 0, '', '.') }}</span></li>
            <li class="list-group-item" style="background-color: #fafafa;">Not Approve <span class="badge" style="background-color: indianred;">Rp. {{ number_format($total_bni_kredit_inapprove, 0, '', '.') }}</span></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="panel panel-default">
        <div class="panel-heading">BRI</div>
        <div class="panel-body">
          <ul class="list-group">
            <li class="list-group-item">Total Pembelian <span class="badge">Rp. {{ number_format($total_bri, 0, '', '.') }}</span></li>
            <li class="list-group-item">Debit <span class="badge">Rp. {{ number_format($total_bri_debit, 0, '', '.') }}</span></li>

            <li class="list-group-item" style="background-color: #fafafa;">Approve <span class="badge" style="background-color: steelblue">Rp. {{ number_format($total_bri_debit_approve, 0, '', '.') }}</span></li>
            <li class="list-group-item" style="background-color: #fafafa;">Not Approve <span class="badge" style="background-color: indianred;">Rp. {{ number_format($total_bri_debit_inapprove, 0, '', '.') }}</span></li>

            <li class="list-group-item">Kredit <span class="badge">Rp. {{ number_format($total_bri_kredit, 0, '', '.') }}</span></li>

            <li class="list-group-item" style="background-color: #fafafa;">Approve <span class="badge" style="background-color: steelblue">Rp. {{ number_format($total_bri_kredit_approve, 0, '', '.') }}</span></li>
            <li class="list-group-item" style="background-color: #fafafa;">Not Approve <span class="badge" style="background-color: indianred;">Rp. {{ number_format($total_bri_kredit_inapprove, 0, '', '.') }}</span></li>
          </ul>
        </div>
      </div>
    </div>
  </div>

</div>

@stop

