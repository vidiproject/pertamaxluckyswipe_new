@extends('layouts.admin')
@section('content')
<div class="container">
  <h3>Receipts</h3>
  <div class="row">
    <div class="col-md-12">
      <form method="post" action="{{ url('admin/receipt/update') }}"> 
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="id" value="{{ $receipt->id }}">
        <div class="form-group">
          <label>Nominal</label>
          <input type="text" name="nominal" class="form-control" value="{{ $receipt->nominal }}">
        </div>
        <div class="form-group">
          <label>Poin</label>
          <input type="text" name="poin" class="form-control" value="{{ $receipt->poin }}">
        </div>
        <div class="form-group">
          <input type="hidden" name="previous_url" value="{{ url()->previous() }}">
          <button type="submit" class="btn btn-primary">Update</button>
        </div>
      </form>
    </div>
  </div>
</div>
@stop