@extends('layouts.admin')
@section('content')
<div class="container">
  <h3>Point {{ $point->user->name }}</h3>
  <div class="row">
    <div class="col-md-12">
      <form method="post" action="{{ url('admin/point/update') }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="id" value="{{ $point->id }}">

        <div class="row">
          <div class="form-group col-md-6">
            <label>Poin Mandiri</label>
            <input type="text" name="mandiri_poin" class="form-control" value="{{ $point->mandiri_poin }}">
          </div>
          <div class="form-group col-md-6">
            <label>Kesempatan Mandiri</label>
            <input type="text" name="mandiri_chance" class="form-control" value="{{ $point->mandiri_chance }}">
          </div>
        </div>

        <div class="row">
          <div class="form-group col-md-6">
            <label>Poin BNI</label>
            <input type="text" name="bni_poin" class="form-control" value="{{ $point->bni_poin }}">
          </div>
          <div class="form-group col-md-6">
            <label>Kesempatan BNI</label>
            <input type="text" name="bni_chance" class="form-control" value="{{ $point->bni_chance }}">
          </div>
        </div>

        <div class="row">
          <div class="form-group col-md-6">
            <label>Poin BRI</label>
            <input type="text" name="bri_poin" class="form-control" value="{{ $point->bri_poin }}">
          </div>
          <div class="form-group col-md-6">
            <label>Kesempatan BRI</label>
            <input type="text" name="bri_chance" class="form-control" value="{{ $point->bri_chance }}">
          </div>
        </div>

        <div class="row">
          <div class="form-group col-md-12">
            <button type="submit" class="btn btn-primary">Update</button>
          </div>
        </div>

      </form>
    </div>
  </div>
</div>
@stop