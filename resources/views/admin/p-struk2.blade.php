@extends('layouts.admin')
@section('content')

<div class="container">
<h3>Statistik Data Struk</h3>

<form id="dateRange" method="get" style="margin-bottom: 20px">
  <div class="row">
    <div class="col-md-12">
      <label>Date Range</label>
    </div>
    <div class="col-md-3">
      <input id="startDate" type="text" name="startDate" value="{{ Request::get('startDate') }}" placeholder="2017-01-01" class="form-control">
    </div>
    <div class="col-md-3">
      <input id="endDate" type="text" name="endDate" value="{{ Request::get('endDate') }}" placeholder="2017-01-31" class="form-control">
    </div>
    <div class="col-md-2">
      <button type="submit" class="btn btn-primary">Filter</button>
    </div> 
  </div>   
</form>

<div class="row">
  <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Grafik User Upload Semua Bank
        </div>
        <div class="panel-body">
          <div id="total-container" style="height: 250px;"></div>
        </div>
      </div>
  </div>
</div>

<div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          BNI
        </div>
        <div class="panel-body">
          <div class="col-md-12">
              <h4>Grafik User BNI</h4>
              <div id="total-bni-container" style="height: 250px;"></div>
          </div>
          <div class="col-md-6">
              <h4>Grafik User BNI - Debit</h4>
              <div id="total-bni-debit-container" style="height: 250px;"></div>
          </div>
          <div class="col-md-6">
              <h4>Grafik User BNI - Kredit</h4>
              <div id="total-bni-kredit-container" style="height: 250px;"></div>
          </div>
        </div>
      </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          BRI
        </div>
        <div class="panel-body">
          <div class="col-md-12">
              <h4>Grafik User BRI</h4>
              <div id="total-bri-container" style="height: 250px;"></div>
          </div>
          <div class="col-md-6">
              <h4>Grafik User BRI - Debit</h4>
              <div id="total-bri-debit-container" style="height: 250px;"></div>
          </div>
          <div class="col-md-6">
              <h4>Grafik User BRI - Kredit</h4>
              <div id="total-bri-kredit-container" style="height: 250px;"></div>
          </div>
        </div>
      </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Mandiri
        </div>
        <div class="panel-body">
          <div class="col-md-12">
              <h4>Grafik User Mandiri</h4>
              <div id="total-mandiri-container" style="height: 250px;"></div>
          </div>
          <div class="col-md-6">
              <h4>Grafik User Mandiri - Debit</h4>
              <div id="total-mandiri-debit-container" style="height: 250px;"></div>
          </div>
          <div class="col-md-6">
              <h4>Grafik User Mandiri - Kredit</h4>
              <div id="total-mandiri-kredit-container" style="height: 250px;"></div>
          </div>
        </div>
      </div>
    </div>
</div>
  
</div>

@stop
@section('stylesheet')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
@stop
@section('script')

<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>

<script>

    $('#startDate').datepicker({
      format: 'yyyy-mm-dd',
      //startDate: '-3d'
    });

    $('#endDate').datepicker({
      format: 'yyyy-mm-dd',
      //startDate: '-3d'
    });

    var total = {!! $total !!};

    new Morris.Line({
        // ID of the element in which to draw the chart.
        element: 'total-container',
        data: total,
        xkey: 'date',
        ykeys: ['value'],
        labels: ['Users'],
        parseTime: false
    });

    // mandiri
    new Morris.Line({
        // ID of the element in which to draw the chart.
        element: 'total-mandiri-container',
        data: {!! $total_mandiri !!},
        xkey: 'date',
        ykeys: ['value'],
        labels: ['Users'],
        parseTime: false
    });


    new Morris.Line({
        // ID of the element in which to draw the chart.
        element: 'total-mandiri-debit-container',
        data: {!! $total_mandiri_debit !!},
        xkey: 'date',
        ykeys: ['value'],
        labels: ['Users'],
        parseTime: false
    });

    new Morris.Line({
        // ID of the element in which to draw the chart.
        element: 'total-mandiri-kredit-container',
        data: {!! $total_mandiri_kredit !!},
        xkey: 'date',
        ykeys: ['value'],
        labels: ['Users'],
        parseTime: false
    });


    // bni
    new Morris.Line({
        // ID of the element in which to draw the chart.
        element: 'total-bni-container',
        data: {!! $total_bni !!},
        xkey: 'date',
        ykeys: ['value'],
        labels: ['Users'],
        parseTime: false
    });


    new Morris.Line({
        // ID of the element in which to draw the chart.
        element: 'total-bni-debit-container',
        data: {!! $total_bni_debit !!},
        xkey: 'date',
        ykeys: ['value'],
        labels: ['Users'],
        parseTime: false
    });

    new Morris.Line({
        // ID of the element in which to draw the chart.
        element: 'total-bni-kredit-container',
        data: {!! $total_bni_kredit !!},
        xkey: 'date',
        ykeys: ['value'],
        labels: ['Users'],
        parseTime: false
    });


    // bri
    new Morris.Line({
        // ID of the element in which to draw the chart.
        element: 'total-bri-container',
        data: {!! $total_bri !!},
        xkey: 'date',
        ykeys: ['value'],
        labels: ['Users'],
        parseTime: false
    });


    new Morris.Line({
        // ID of the element in which to draw the chart.
        element: 'total-bri-debit-container',
        data: {!! $total_bri_debit !!},
        xkey: 'date',
        ykeys: ['value'],
        labels: ['Users'],
        parseTime: false
    });

    new Morris.Line({
        // ID of the element in which to draw the chart.
        element: 'total-bri-kredit-container',
        data: {!! $total_bri_kredit !!},
        xkey: 'date',
        ykeys: ['value'],
        labels: ['Users'],
        parseTime: false
    });
</script>

@stop