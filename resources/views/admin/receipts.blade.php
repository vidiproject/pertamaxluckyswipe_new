@extends('layouts.admin')
@section('content')
<div class="container">
  <h3>Receipts</h3>
  <div class="row">
    <div class="col-md-12">
      <form method="post" action="{{ url('/admin/receipt') }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
          <label>Search by ID</label>
          <input type="text" name="key" class="form-control" placeholder="Type name or email then type enter">
        </div>
      </form>
      <table class="table">
        <thead>
          <tr>
              <th>ID</th>
              <th>User</th>
              <th style="width: 12%;">Date</th>
              <th>No SPBU</th>
              <th>Gasoline</th>
              <th>Nominal</th>
              <th>Bank</th>
              <th>Payment</th>
              <th>BIN</th>
              <th>Status</th>
              <th>Point</th>
              <th>App Code</th>
              <th>Edit</th>
          </tr>
        </thead>
        <tbody>
          @foreach($receipts as $receipt)
          <tr>
            <td>{{ $receipt->id }}</td>
            <td><small>{{ str_limit($receipt->user->name, 20) }}</small></td>
            <td>{{ $receipt->created_at->format('d-M-y h:i') }}</td>
            <td>{{ $receipt->nospbu }}</td>
            <td class="text-capitalize">{{ $receipt->bensin }}</td>
            <td>Rp. {{ number_format( $receipt->nominal , 0 , '' , '.' ) }}</td>
            <td class="text-capitalize">{{ $receipt->namabank }}</td>
            <td class="text-capitalize">{{ $receipt->pembayaran }}</td>
            <td>{{ $receipt->nocc }}</td>
            <td>{{ $receipt->status }}</td>
            <td>{{ $receipt->poin }}</td>
            <td>{{ $receipt->approvalcode }}</td>
            <td><a href="{{ url('admin/receipt/edit/'. $receipt->id) }}">Edit</a></td>
          </tr>
          @endforeach
        </tbody>
      </table>

      {{ $receipts->links() }}

    </div>
  </div>
</div>
@stop