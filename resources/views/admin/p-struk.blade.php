@extends('layouts.admin')
@section('content')

<div class="container">
  <h3>Statistik Data Struk</h3>


  <form id="dateRange" method="get" style="margin-bottom: 20px">
    <div class="row">
      <div class="col-md-12">
        <label>Date Range</label>
      </div>
      <div class="col-md-3">
        <input id="from" type="text" name="from" value="{{ Request::get('from') }}" placeholder="2017-01-01" class="form-control">
      </div>
      <div class="col-md-3">
        <input id="to" type="text" name="to" value="{{ Request::get('to') }}" placeholder="2017-01-31" class="form-control">
      </div>
      <div class="col-md-2">
        <button type="submit" class="btn btn-primary">Filter</button>
      </div> 
    </div>   
  </form>

  <div class="row">
    <div class="col-md-4">
      <div class="panel panel-default">
        <div class="panel-heading">Mandiri</div>
        <div class="panel-body">
          <ul class="list-group">
            <li class="list-group-item">Total Struk <span class="badge">{{ $total_mandiri }}</span></li>
            <li class="list-group-item">Debit <span class="badge">{{ $total_mandiri_debit }}</span></li>
            <li class="list-group-item" style="background-color: #eee">Approve <span class="badge" style="background-color: steelblue;">{{ $total_mandiri_debit_approve }}</span></li>
            <li class="list-group-item" style="background-color: #eee">Not Approve <span class="badge" style="background-color: indianred;">{{ $total_mandiri_debit_inapprove }}</span></li>
            <li class="list-group-item">Kredit <span class="badge">{{ $total_mandiri_kredit }}</span></li>
            <li class="list-group-item" style="background-color: #eee">Approve <span class="badge" style="background-color: steelblue;">{{ $total_mandiri_kredit_approve }}</span></li>
            <li class="list-group-item" style="background-color: #eee">Not Approve <span class="badge" style="background-color: indianred;">{{ $total_mandiri_kredit_inapprove }}</span></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="panel panel-default">
        <div class="panel-heading">BNI</div>
        <div class="panel-body">
          <ul class="list-group">
            <li class="list-group-item">Total Struk <span class="badge">{{ $total_bni }}</span></li>
            <li class="list-group-item">Debit <span class="badge">{{ $total_bni_debit }}</span></li>
            <li class="list-group-item" style="background-color: #eee">Approve <span class="badge" style="background-color: steelblue;">{{ $total_bni_debit_approve }}</span></li>
            <li class="list-group-item" style="background-color: #eee">Not Approve <span class="badge" style="background-color: indianred;">{{ $total_bni_debit_inapprove }}</span></li>
            <li class="list-group-item">Kredit <span class="badge">{{ $total_bni_kredit }}</span></li>
            <li class="list-group-item" style="background-color: #eee">Approve <span class="badge" style="background-color: steelblue;">{{ $total_bni_kredit_approve }}</span></li>
            <li class="list-group-item" style="background-color: #eee">Not Approve <span class="badge" style="background-color: indianred;">{{ $total_bni_kredit_inapprove }}</span></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="panel panel-default">
        <div class="panel-heading">BRI</div>
        <div class="panel-body">
          <ul class="list-group">
            <li class="list-group-item">Total Struk <span class="badge">{{ $total_bri }}</span></li>
            <li class="list-group-item">Debit <span class="badge">{{ $total_bri_debit }}</span></li>
            <li class="list-group-item" style="background-color: #eee">Approve <span class="badge" style="background-color: steelblue;">{{ $total_bri_debit_approve }}</span></li>
            <li class="list-group-item" style="background-color: #eee">Not Approve <span class="badge" style="background-color: indianred;">{{ $total_bri_debit_inapprove }}</span></li>
            <li class="list-group-item">Kredit <span class="badge">{{ $total_bri_kredit }}</span></li>
            <li class="list-group-item" style="background-color: #eee">Approve <span class="badge" style="background-color: steelblue;">{{ $total_bri_kredit_approve }}</span></li>
            <li class="list-group-item" style="background-color: #eee">Not Approve <span class="badge" style="background-color: indianred;">{{ $total_bri_kredit_inapprove }}</span></li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div id="chartContainer" style="height: 300px; width: 100%;"></div>
    </div>
  </div>


  <br><br><br>

</div>

@stop
@section('script')
<script type="text/javascript">

  window.onload = function () {
    var chart = new CanvasJS.Chart("chartContainer",
    {
      title:{
        text: "Graph"
      },
      animationEnabled: true,
      legend: {
        cursor:"pointer",
        itemclick : function(e) {
          if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
              e.dataSeries.visible = false;
          }
          else {
              e.dataSeries.visible = true;
          }
          chart.render();
        }
      },
      axisY: {
        title: "Amount"
      },
      toolTip: {
        shared: true,  
        content: function(e){
          var str = '';
          var total = 0 ;
          var str3;
          var str2 ;
          for (var i = 0; i < e.entries.length; i++){
            var  str1 = "<span style= 'color:"+e.entries[i].dataSeries.color + "'> " + e.entries[i].dataSeries.name + "</span>: <strong>"+  e.entries[i].dataPoint.y + "</strong> <br/>" ; 
            total = e.entries[i].dataPoint.y + total;
            str = str.concat(str1);
          }
          str2 = "<span style = 'color:DodgerBlue; '><strong>"+e.entries[0].dataPoint.label + "</strong></span><br/>";
          str3 = "<span style = 'color:Tomato '>Total: </span><strong>" + total + "</strong><br/>";
          
          return (str2.concat(str)).concat(str3);
        }

      },
      data: [
      {        
        type: "bar",
        showInLegend: true,
        name: "Total",  
        color: "#d32f2f",
        dataPoints: [
        { y: {{ $total_mandiri }}, label: "Mandiri"},
        { y: {{ $total_bni }}, label: "BNI"},
        { y: {{ $total_bri }}, label: "BRI"}
        ]
      },
      {        
        type: "bar",
        showInLegend: true,
        name: "Debit",
        color: "#283593",          
        dataPoints: [
        { y: {{ $total_mandiri_debit }}, label: "Mandiri"},
        { y: {{ $total_bni_debit }}, label: "BNI"},
        { y: {{ $total_bri_debit }}, label: "BRI"}
        ]
      },
      {        
        type: "bar",
        showInLegend: true,
        name: "Kredit",
        color: "#ffa000",
        dataPoints: [
        { y: {{ $total_mandiri_kredit }}, label: "Mandiri"},
        { y: {{ $total_bni_kredit }}, label: "BNI"},
        { y: {{ $total_bri_kredit }}, label: "BRI"}
        ]
      }

      ]
    });

chart.render();
}



    $('#from').datepicker({
      format: 'yyyy-mm-dd',
      //startDate: '-3d'
    });

    $('#to').datepicker({
      format: 'yyyy-mm-dd',
      //startDate: '-3d'
    });

</script>
@stop