@extends('layouts.admin')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <form action="{{ url('admin/user/update') }}" method="post">

      <span class="text-success">
        {{ Session::get('message') }}
      </span>

        <div class="row">
          <div class="form-group col-md-12">
            <label for="name">Name</label>
            <input id="name" type="text" name="name" value="{{ $user->name }}" class="form-control">

            @if ($errors->has('name'))
                <p class="error">{{ $errors->first('name') }}</p>
            @endif
          </div>
        </div>
        <div class="row">
          <div class="form-group col-md-12">
            <label for="email">Email</label>
            <input id="email" type="email" name="email" value="{{ $user->email }}" class="form-control">

            @if ($errors->has('email'))
            <span class="help-block">
                <p class="error">{{ $errors->first('email') }}</p>
            </span>
            @endif
          </div>
        </div>
        <div class="row">
          <div class="form-group col-md-12">
            <label>No KTP</label>
            <input type="text" name="ktp" value="{{ $user->ktp }}" class="form-control">

            @if ($errors->has('ktp'))
            <span class="help-block">
                <p class="error">{{ $errors->first('ktp') }}</p>
            </span>
            @endif
          </div>
        </div>
        <div class="row">
          <div class="form-group col-md-12">
            <label>No Handphone</label>
            <input type="number" name="phone" value="{{ $user->phone }}" class="form-control">

            @if ($errors->has('phone'))
            <span class="help-block">
                <p class="error">{{ $errors->first('phone') }}</p>
            </span>
            @endif
          </div>
        </div>
        <div class="row">
          <div class="form-group col-md-6">
            <label>User Role</label>
            <select name="role" class="form-control">
              <option value="1" {{ ($user->role == 1 ? 'selected' : '') }}>Admin</option>
              <option value="2" {{ ($user->role == 2 ? 'selected' : '') }}>Moderator</option>
              <option value="0" {{ ($user->role == 0 ? 'selected' : '') }}>Customer</option>
            </select>

            @if ($errors->has('role'))
                <p class="error">{{ $errors->first('role') }}</p>
            @endif
          </div>
          <div class="form-group col-md-6">
            <label>Moderator Bank</label>
            <select name="bank" class="form-control">
              <option value="" disabled {{ ($user->handle == null ? 'selected' : '') }}>Choose bank</option>
              <option value="bni" {{ ($user->handle == 'bni' ? 'selected' : '') }}>BNI</option>
              <option value="bri" {{ ($user->handle == 'bri' ? 'selected' : '') }}>BRI</option>
              <option value="mandiri" {{ ($user->handle == 'mandiri' ? 'selected' : '') }}>Mandiri</option>
            </select>

            @if ($errors->has('bank'))
                <p class="error">{{ $errors->first('bank') }}</p>
            @endif
          </div>
        </div>
        <input type="hidden" name="previous_url" value="{{ url()->previous() }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="id" value="{{ $user->id }}">
        <button type="sumbit" name="button" class="btn btn-primary">Submit</button>
      </form>
    </div>
  </div>
</div>

@stop
