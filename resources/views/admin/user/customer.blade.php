@extends('layouts.admin')
@section('content')
<div class="container">
  <h3>Customers <span class="badge badge-default">{{ $users->count() }}/1</span></h3>
  <div class="row">
    <div class="col-md-12">
      <form method="post" action="{{ url('/admin/user/customer') }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
          <label>Search by Name or Email</label>
          <input type="text" name="key" class="form-control" placeholder="Type name or email then type enter">
        </div>
      </form>
      <table class="table">
        <thead>
          <tr>
              <th>ID</th>
              <td>Date</td>
              <th data-field="name">Name</th>
              <th data-field="email">Email</th>
              <th>Phone</th>
              <!-- <th>ID Card</th> -->
              <th>IP</th>
              <th data-field="role">MAN/BNI/BRI</th>
              <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @foreach($users as $user)
          <tr>
            <td>{{ $user->id }}</td>
            <td><small>{{ $user->created_at->format('d/m/Y h:i:s') }}</small></td>
            <td><a href="{{ url('admin/user/customer/'. $user->id) }}">{{ $user->name }}</a></td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->phone }}</td>
            <!-- <td>{{ $user->ktp }}</td> -->
            <td>{{ $user->ip }}</td>
            <td>{{ $user->point->mandiri_poin }}/{{ $user->point->bni_poin }}/{{ $user->point->bri_poin }}</td>
            <td><a href="{{ url('admin/user/edit/'. $user->id) }}">Edit</a></td>
          </tr>
          @endforeach
        </tbody>
      </table>

      {{ $users->links() }}
    </div>
  </div>
</div>
@stop