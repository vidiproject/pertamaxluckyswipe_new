@extends('layouts.admin')
@section('content')
<div class="container">
  <h3>Users</h3>
  <div class="row">
    <div class="col-md-12">
      <table class="table">
        <thead>
          <tr>
              <th>ID</th>
              <th data-field="name">Name</th>
              <th data-field="email">Email</th>
              <th>Phone</th>
              <th>No KTP</th>
              <th data-field="role">Role</th>
              <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @foreach($users as $user)
          <tr>
            <td>{{ $user->id }}</td>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->phone }}</td>
            <td>{{ $user->ktp }}</td>
            <td>{{ $user->role }}</td>
            <td><a href="{{ url('admin/user/edit/'. $user->id) }}">Edit</a></td>
          </tr>
          @endforeach
        </tbody>
      </table>

      {{ $users->links() }}
    </div>
  </div>
</div>
@stop
