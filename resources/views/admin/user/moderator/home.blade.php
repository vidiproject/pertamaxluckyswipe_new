@extends('layouts.admin')
@section('content')

<div class="container">
    <h3>Moderators</h3>
    <table class="table">
      <thead>
        <tr>
            <th>ID</th>
            <th data-field="name">Name</th>
            <th data-field="email">Email</th>
            <th data-field="role">Handle</th>
            <th>Action</th>
        </tr>
      </thead>
      <tbody>
        @foreach($moderators as $user)
        <tr>
          <td>{{ $user->id }}</td>
          <td>{{ $user->name }}</td>
          <td>{{ $user->email }}</td>
          <td>{{ $user->handle }}</td>
          <td><a href="{{ url('admin/user/edit/'. $user->id) }}">Edit</a></td>
        </tr>
        @endforeach
      </tbody>
    </table>

</div>

@stop
