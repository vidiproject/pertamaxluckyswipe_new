@extends('layouts.admin')
@section('content')

<div class="container">
  <h3>Statistics</h3>
  <div class="row">
    <div class="col-md-4">
      <div class="panel panel-default">
        <div class="panel-heading">Admin</div>
        <div class="panel-body text-center">
          <h4>{{ $admins }}</h4>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="panel panel-default">
        <div class="panel-heading">Moderators</div>
        <div class="panel-body text-center">
          <h4>{{ $moderators }}</h4>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="panel panel-primary">
        <div class="panel-heading">Customers</div>
        <div class="panel-body text-center">
          <h4>{{ $users }}</h4>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-4">
      <div class="panel panel-default">
        <div class="panel-heading"><a href="{{ url('/admin/receipt') }}">Total Receipts</a></div>
        <div class="panel-body text-center">
          <h4>{{ $receipts }}</h4>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="panel panel-default">
        <div class="panel-heading">Receipts Approved</div>
        <div class="panel-body text-center">
          <h4>{{ $receipts_approved }}</h4>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="panel panel-danger">
        <div class="panel-heading">Receipts Disapproved</div>
        <div class="panel-body text-center">
          <h4>{{ $receipts_disapproved }}</h4>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-4">
      <div class="panel panel-info">
        <div class="panel-heading">Receipts BNI <a href="{{ url('admin/export/receipts/bni') }}" class="pull-right">Export csv</a></div>
        <div class="panel-body text-center">
          <h4>{{ $receipts_bni }}</h4>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="panel panel-info">
        <div class="panel-heading">Receipts BRI <a href="{{ url('admin/export/receipts/bri') }}" class="pull-right">Export csv</a></div>
        <div class="panel-body text-center">
          <h4>{{ $receipts_bri }}</h4>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="panel panel-info">
        <div class="panel-heading">Receipts Mandiri <a href="{{ url('admin/export/receipts/mandiri') }}" class="pull-right">Export csv</a></div>
        <div class="panel-body text-center">
          <h4>{{ $receipts_mandiri }}</h4>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-4">
      <div class="panel panel-success">
        <div class="panel-heading">Receipts BNI Approved</div>
        <div class="panel-body text-center">
          <h4>{{ $receipts_bni_approved }}</h4>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="panel panel-success">
        <div class="panel-heading">Receipts BRI Approved</div>
        <div class="panel-body text-center">
          <h4>{{ $receipts_bri_approved }}</h4>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="panel panel-success">
        <div class="panel-heading">Receipts Mandiri Approved</div>
        <div class="panel-body text-center">
          <h4>{{ $receipts_mandiri_approved }}</h4>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-4">
      <div class="panel panel-default">
        <div class="panel-heading">Avarage Value of Receipts</div>
        <div class="panel-body text-center">
          <h4>{{ number_format( $receipts_count , 0 , '' , '.' ) }}</h4>
        </div>
      </div>
    </div>
  </div>

</div>

@stop
