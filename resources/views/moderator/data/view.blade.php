@extends('layouts.moderator')
@section('content')

<div class="row">

	<div class="col-md-12">
		<form id="dateRange" method="post" style="margin-bottom: 20px">
		    <div class="row">
		      <div class="col-md-12">
		        <label>Pilih Tanggal</label>
		      </div>
		      <div class="col-md-3">
		        <input id="from" type="text" name="from" value="{{ Request::get('from') }}" placeholder="2017-01-01" class="form-control" required>
		      </div>
		      <div class="col-md-3">
		        <input id="to" type="text" name="to" value="{{ Request::get('to') }}" placeholder="2017-01-31" class="form-control" required>
		      </div>
			    <div class="col-md-3">
			    	<input type="hidden" name="_token" value="{{ csrf_token() }}">
			    	<input type="hidden" name="bank" value="mandiri">
			    	<button type="submit" class="btn btn-primary">Export Mandiri</button>
			    </div>
		    </div>
	  </form>
	</div>


	<div class="col-md-12">
		<form id="dateRange" method="post" style="margin-bottom: 20px">
		    <div class="row">
		      <div class="col-md-12">
		        <label>Pilih Tanggal</label>
		      </div>
		      <div class="col-md-3">
		        <input id="from2" type="text" name="from" value="{{ Request::get('from') }}" placeholder="2017-01-01" class="form-control" required>
		      </div>
		      <div class="col-md-3">
		        <input id="to2" type="text" name="to" value="{{ Request::get('to') }}" placeholder="2017-01-31" class="form-control" required>
		      </div>
			    <div class="col-md-3">
			    	<input type="hidden" name="_token" value="{{ csrf_token() }}">
			    	<input type="hidden" name="bank" value="bni">
			    	<button type="submit" class="btn btn-primary">Export BNI</button>
			    </div>
		    </div>
	  </form>
	</div>

	<div class="col-md-12">
		<form id="dateRange" method="post" style="margin-bottom: 20px">
		    <div class="row">
		      <div class="col-md-12">
		        <label>Pilih Tanggal</label>
		      </div>
		      <div class="col-md-3">
		        <input id="from3" type="text" name="from" value="{{ Request::get('from') }}" placeholder="2017-01-01" class="form-control" required>
		      </div>
		      <div class="col-md-3">
		        <input id="to3" type="text" name="to" value="{{ Request::get('to') }}" placeholder="2017-01-31" class="form-control" required>
		      </div>
			    <div class="col-md-3">
			    	<input type="hidden" name="_token" value="{{ csrf_token() }}">
			    	<input type="hidden" name="bank" value="bri">
			    	<button type="submit" class="btn btn-primary">Export BRI</button>
			    </div>
		    </div>
	  </form>
	</div>

</div>

@stop
@section('script')

<script type="text/javascript">
	$('#from').datepicker({
      format: 'yyyy-mm-dd',
      //startDate: '-3d'
    });

    $('#to').datepicker({
      format: 'yyyy-mm-dd',
      //startDate: '-3d'
    });

    $('#from2').datepicker({
      format: 'yyyy-mm-dd',
      //startDate: '-3d'
    });

    $('#to2').datepicker({
      format: 'yyyy-mm-dd',
      //startDate: '-3d'
    });

    $('#from3').datepicker({
      format: 'yyyy-mm-dd',
      //startDate: '-3d'
    });

    $('#to3').datepicker({
      format: 'yyyy-mm-dd',
      //startDate: '-3d'
    });
</script>

@stop