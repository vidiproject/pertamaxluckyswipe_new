@extends('layouts.moderator')
@section('content')



<div class="panel panel-default">
  <div class="panel-heading">Lengkapi Data <a href="#">{{ $receipt->user->name }}</a></div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-6">
        <img src="{{ $receipt->struk1 }}" class="img-responsive">
      </div>
      <div class="col-md-6">
        <img src="{{ $receipt->struk2 }}" class="img-responsive"> 
      </div>
    </div>
    <br>
    <form action="{{ url('moderator/update') }}" method="post">
      <div class="row">
        <div class="form-group col-md-4">
          <label for="name">Nama Customer</label>
          <input id="name" name="name" class="form-control" type="text" value="{{ $receipt->user->name }}" class="validate" disabled>
        </div>
        <div class="form-group col-md-4 {{ ( $errors->has('nospbu') ? 'has-error' : '' ) }}">
          <label for="name">No SPBU</label>
          <input id="nospbu" name="nospbu" class="form-control" type="text" value="{{ $receipt->nospbu }}" class="validate">
          @if ($errors->has('nospbu'))
              <span class="help-block text-danger">{{ $errors->first('nospbu') }}</span>
          @endif
        </div>
        <div class="form-group col-md-4 {{ ( $errors->has('lastfourdigits') ? 'has-error' : '' ) }}">
          <label class="control-label">4 Digit Terakhir</label>
          <input type="text" name="lastfourdigits" class="form-control" value="{{ ( old('lastfourdigits') == '' ? $receipt->lastfourdigits : old('lastfourdigits') ) }}">
          @if ($errors->has('lastfourdigits'))
              <span class="help-block text-danger">{{ $errors->first('lastfourdigits') }}</span>
          @endif
        </div>
      </div>
      <div class="row">
          <div class="form-group col-md-4 {{ ( $errors->has('nominal') ? 'has-error' : '' ) }}">
            <label class="control-label" for="nominal">Total Harga Bensin</label>
            <input id="nominal" class="form-control" type="text" name="nominal" value="{{ $receipt->nominal }}" class="validate">
            @if ($errors->has('nominal'))
                <span class="help-block text-danger">{{ $errors->first('nominal') }}</span>
            @endif
          </div>
          <div class="form-group col-md-4 {{ ( $errors->has('bensin') ? 'has-error' : '' ) }}">
            <label class="control-label">Jenis Bensin</label>
            <select name="bensin" class="form-control">
              <option value="" disabled selected>Pilih jenis bensin</option>
              <option value="pertamax" {{ ($receipt->bensin == 'pertamax' ? 'selected' : '') }}>Pertamax</option>
              <option value="pertamax plus" {{ ($receipt->bensin == 'pertamax plus' ? 'selected' : '') }}>Pertamax Plus</option>
              <option value="pertamax turbo" {{ ($receipt->bensin == 'pertamax turbo' ? 'selected' : '') }}>Pertamax Turbo</option>
              <option value="pertalite" {{ ($receipt->bensin == 'pertalite' ? 'selected' : '') }}>Pertalite</option>
              <option value="pertamina dex" {{ ($receipt->bensin == 'pertamina dex' ? 'selected' : '') }}>Pertamina Dex</option>
              <option value="dexlite" {{ ($receipt->bensin == 'dexlite' ? 'selected' : '') }}>Dexlite</option>
            </select>
            @if ($errors->has('bensin'))
                <span class="help-block text-danger">{{ $errors->first('bensin') }}</span>
            @endif
          </div>
          <div class="form-group col-md-4 {{ ( $errors->has('approvalcode') ? 'has-error' : '' ) }}">
            <label class="control-label" for="approvalcode">Approval Code</label>
            <input id="approvalcode" class="form-control" type="text" name="approvalcode" value="{{ $receipt->approvalcode }}" class="validate">
            @if ($errors->has('approvalcode'))
                <span class="help-block text-danger">{{ $errors->first('approvalcode') }}</span>
            @endif
          </div>
          <div class="col-md-12">
            <?php
              if ($receipt->status == 0) {
                $status_struk = 'Waiting';
              } elseif ($receipt->status == 1) {
                $status_struk = 'Approved';
              } else {
                $status_struk = 'Declined';
              }
            ?>
            <label>Status: {{ $status_struk }}</label><br>
            <label>Tanggal : {{ $receipt->created_at->format('d/m/y') }}</label><br>
            <label>ID : {{ $receipt->id }}</label><br>
            <label>BANK : {{ $receipt->namabank }}</label>
            <br><br>
          </div>
      </div>
      <div class="row">
        <div class="form-group col-md-12">
          @if(Session::has('error'))
            <div class="alert alert-danger" role="alert">{{ Session::get('error') }}</div>
          @endif
          @if(Session::has('message'))
            <div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
          @endif
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="hidden" name="id" value="{{ $receipt->id }}">
          <button type="submit" class="btn btn-primary" name="button">Simpan</button>
          <button type="button" onclick="back();" class="btn btn-warning">Kembali</button>
          <button type="button" onclick="deleteAja();" class="btn btn-danger">Tolak</button>
          <script type="text/javascript">
              var prev_location = getCookie("previous_url");

              function back()
              {
                $(location).attr('href', prev_location);
              }

              function deleteAja()
              {
                $('#hapus').submit();
              }
              // window.setTimeout(function(){
              //   $(location).attr('href', prev_location);
              // }, 300);
            </script>
        </div>
      </div>
    </form>

    <form id="hapus" action="{{ url('moderator/approval/decline') }}" method="post" style="display: none">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <input type="hidden" name="id" value="{{ $receipt->id }}">
    </form>


    <p><i>* Struk yang mungkin sama:</i></p>

    <table class="table table-responsive table-bordered">
      <thead>
        <tr>
          <th>ID</th>
          <th>Tanggal</th>
          <th>Pemilik</th>
          <th>Di Upload</th>
          <th>Harga</th>
          <th>App Code</th>
          <th>Struk A</th>
          <th>Struk B</th>
          <th>Bank</th>
          <th>Lihat</th>
        </tr>
      </thead>
      <tbody>
        @if($related_receipts != null)
          @foreach($related_receipts as $item)
            <tr>
              <td>{{ $item->id }}</td>
              <td>{{ $item->created_at->format('d/m/y') }}</td>
              <td>{{ $item->user->name }}</td>
              <td>{{ $item->created_at->format('d/m/y h:i A') }}</td>
              <td>{{ $item->nominal }}</td>
              <td>{{ $item->approvalcode }}</td>
              <td>
                <a href="#">
                  <img data-toggle="modal" data-target="#struk1{{ $item->id }}" src="{{ $item->struk1 }}" style="width: 30px;height: 30px; cursor: zoom-in;" alt="...">
                </a>
                <!-- Modal -->
                <div class="modal fade" id="struk1{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      </div>
                      <div class="modal-body">
                        <img data-toggle="modal" data-target="#struk1{{ $item->id }}" src="{{ $item->struk1 }}" class="img-responsive" alt="...">
                      </div>
                    </div>
                  </div>
                </div>
              </td>
              <td>
                <a href="#">
                  <img data-toggle="modal" data-target="#struk2{{ $item->id }}" src="{{ $item->struk2 }}" style="width: 30px;height: 30px; cursor: zoom-in;" alt="...">
                </a>
                <!-- Modal -->
                <div class="modal fade" id="struk2{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      </div>
                      <div class="modal-body">
                        <img data-toggle="modal" data-target="#struk2{{ $item->id }}" src="{{ $item->struk2 }}" class="img-responsive" alt="...">
                      </div>
                    </div>
                  </div>
                </div>
              </td>
              <td>{{ $receipt->namabank }}</td>
              <td><a target="_blank" href="{{ url('moderator/check/edit/'. $item->id) }}">Lihat</a></td>
            </tr>
          @endforeach
        @else
        <tr>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
        </tr>
        @endif
      </tbody>
    </table>

  </div>

</div>
@stop