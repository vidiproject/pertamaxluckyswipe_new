@extends('layouts.moderator')
@section('content')

<div class="row">
  <div class="col-md-4">
    <div class="panel panel-default">
      <div class="panel-heading">
        Admin BNI
      </div>
      <div class="panel-body text-center">
        @foreach($admin_bni as $admin)
        <p>{{ $admin->name }}</p>
        @endforeach
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="panel panel-default">
      <div class="panel-heading">
        Admin BRI
      </div>
      <div class="panel-body text-center">
        @foreach($admin_bri as $admin)
        <p>{{ $admin->name }}</p>
        @endforeach
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="panel panel-default">
      <div class="panel-heading">
        Admin Mandiri
      </div>
      <div class="panel-body text-center">
        @foreach($admin_mandiri as $admin)
        <p>{{ $admin->name }}</p>
        @endforeach
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-default">
      <div class="panel-heading">
        Total
      </div>
      <div class="panel-body text-center">
        {{ $receipts }}
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="panel panel-primary">
      <div class="panel-heading">
        Waiting
      </div>
      <div class="panel-body text-center">
        {{ $receipts_waiting }}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-success">
      <div class="panel-heading">
        Approved
      </div>
      <div class="panel-body text-center">
        {{ $receipts_approved }}
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="panel panel-danger">
      <div class="panel-heading">
        Declined
      </div>
      <div class="panel-body text-center">
        {{ $receipts_declined }}
      </div>
    </div>
  </div>
</div>

@stop
