@extends('layouts.moderator')
@section('content')

<form method="post" action="{{ url('moderator/participant') }}">
  {{ csrf_field() }}
  <input type="text" name="key" class="form-control" placeholder="Ketik nama lalu tekan enter..." value="{{ $keyword }}">
</form>

<br>

<table class="table">
  <thead>
    <tr>
        <th>Terdaftar Pada</th>
        <th>Nama</th>
        <th>Email</th>
        <th>Struk</th>
    </tr>
  </thead>

  <tbody>
    @foreach($users as $user)
    <tr>
      <td>{{ $user->created_at->format('d/m/Y h:i A') }}</td>
      <td><a href="{{ url('moderator/participant/'. $user->id) }}">{{ $user->name }}</a></td>
      <td>{{ $user->email }}</td>
      <td class="text-center">{{ $user->receipt->count() }}</td>
    </tr>
    @endforeach
  </tbody>
</table>

{{ $users->links() }}

@stop
