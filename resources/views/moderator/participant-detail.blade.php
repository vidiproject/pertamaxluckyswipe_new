@extends('layouts.moderator')
@section('content')
<div class="container">
  <h3>{{ $user->name }}</h3>
  <p>0 = Waiting; 1 = Approved; 2 = Declined</p>
  <div class="row">
    <div class="col-md-12">
      <table class="table table-responsive">
        <thead>
          <tr>
              <th>ID</th>
              <th>Date</th>
              <th>No SPBU</th>
              <th>Gasoline</th>
              <th>Nominal</th>
              <th>Bank</th>
              <th>Payment</th>
              <th>BIN</th>
              <th>Status</th>
              <th>Point</th>
              <th>App Code</th>
              <th>Last Digit</th>
              <th>Photo</th>
          </tr>
        </thead>
        <tbody>
          <?php $total = 0; ?>
          <?php $total_nominal = 0; ?>
          @foreach($receipts as $receipt)
          <?php $total += $receipt->poin; ?>
          <?php $total_nominal += $receipt->nominal; ?>
          <tr>
            <td>{{ $receipt->id }}</td>
            <td>{{ $receipt->created_at->format('d/m/y') }}</td>
            <td>{{ $receipt->nospbu }}</td>
            <td>{{ $receipt->bensin }}</td>
            <td>{{ number_format( $receipt->nominal , 0 , '' , '.' ) }}</td>
            <td>{{ $receipt->namabank }}</td>
            <td>{{ $receipt->pembayaran }}</td>
            <td>{{ $receipt->nocc }}</td>
            <td>{{ $receipt->status }}</td>
            <td>{{ $receipt->poin }}</td>
            <td>{{ $receipt->approvalcode }}</td>
            <td>{{ $receipt->lastfourdigits }}</td>
            <td>
              <a href="#" data-toggle="modal" data-target="#myModal{{ $receipt->id }}" class="btn btn-default btn-xs">View</a>
              <!-- Modal -->
              <div class="modal fade" id="myModal{{ $receipt->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document" style="width: 90%">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                    </div>
                    <div class="modal-body">
                      <div class="row">
                        <div class="col-md-6">
                          <img src="{{ $receipt->struk1 }}" class="img-responsive">
                        </div>
                        <div class="col-md-6">
                          <img src="{{ $receipt->struk2 }}" class="img-responsive">
                        </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>
            </td>
          </tr>
          @endforeach
          <tr>
            <td><b>Total</b></td>
            <td></td>
            <td></td>
            <td></td>
            <td><b>{{ number_format( $total_nominal , 0 , '' , '.' ) }}</b></td>
            <td></td>
            <td></td>
            <td></td>            
            <td></td>
            <td><b>{{ $total }}</b></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
        </tbody>
      </table>

      {{ $receipts->links() }}


    </div>
  </div>
</div>
@stop