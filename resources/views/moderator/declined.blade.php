@extends('layouts.moderator')
@section('content')

<ul class="nav nav-tabs">
  <li role="presentation"><a href="{{ url('/moderator/approval') }}">Waiting <span class="badge">{{ $waiting }}</span></a></li>
  <li role="presentation"><a href="{{ url('/moderator/approval/approved') }}">Approved <span class="badge">{{ $approved }}</span></a></li>
  <li role="presentation" class="active"><a href="{{ url('/moderator/approval/declined') }}">Declined <span class="badge">{{ $declined }}</span></a></li>
  <li role="presentation"><a href="{{ url('/moderator/approval/eight') }}">Diatas 8 Semua Bank <span class="badge">{{ $eight }}</span></a></li>
</ul>

@if(Session::has('message'))
  <div class="alert alert-success" role="alert" style="margin-top: 10px;">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
    <p>{{ Session::get('message') }}</p>
  </div>
@endif

<table class="table table-responsive table-bordered table-striped">
<br>
<p><i>* Restore akan mengembalikan struk kehalaman waiting / moderasi</i></p>
<p><i>* Hapus akan menghapus data struk dari database</i></p>

  <thead>
    <tr>
        <th></th>
        <th>Diunggah Pada</th>
        <th>Nama</th>
        <th>Bensin</th>
        <th>Harga</th>
        <th>Struk A</th>
        <th>Struk B</th>
        <th>Pembayaran</th>
        <th>App Code</th>
        <th>Poin</th>
        <th>Action</th>
    </tr>
  </thead>

  <tbody>
    @foreach($receipts as $receipt)
    <tr>
      <td><a class="btn btn-default btn-sm" href="{{ url('moderator/check/edit/'. $receipt->id) }}" class="btn btn-link" data-position="left" data-delay="50" data-tooltip="Edit" onclick="callMeMaybe();"><span class="glyphicon glyphicon-pencil"></span></a></td>
      <td><small>{{ $receipt->created_at->format('d/m/y h:i A') }}</small></td>
      <td><small>{{ $receipt->user->name }}</small></td>
      <td style="text-transform: capitalize;"><small>{{ $receipt->bensin }}</small></td>
      <td><small>{{ number_format( $receipt->nominal , 0 , '' , '.' ) }}</small></td>
      <td>
        <a href="#">
          <img data-toggle="modal" data-target="#struk1{{ $receipt->id }}" src="{{ $receipt->struk1 }}" style="width: 30px;height: 30px" alt="...">
        </a>
        
        <div class="modal fade" id="struk1{{ $receipt->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              </div>
              <div class="modal-body">
                <img data-toggle="modal" data-target="#struk1{{ $receipt->id }}" src="{{ $receipt->struk1 }}" class="img-responsive" alt="...">
              </div>
            </div>
          </div>
        </div>
      </td>
      <td>
        <a href="#">
          <img data-toggle="modal" data-target="#struk2{{ $receipt->id }}" src="{{ $receipt->struk2 }}" style="width: 30px;height: 30px" alt="...">
        </a>
        
        <div class="modal fade" id="struk2{{ $receipt->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              </div>
              <div class="modal-body">
                <img data-toggle="modal" data-target="#struk2{{ $receipt->id }}" src="{{ $receipt->struk2 }}" class="img-responsive" alt="...">
              </div>
            </div>
          </div>
        </div>
      </td>
      <td style="text-transform: capitalize;"><small>{{ $receipt->pembayaran }}</small></td>
      <td><small><b>{{ $receipt->approvalcode }}</b></small></td>
      <td>{{ $receipt->poin }}</td>
      <td>
        <div class="btn-group-vertical">
          <form action="{{ url('moderator/receipt/reset') }}" method="post" style="display: inline">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id" value="{{ $receipt->id }}">
            <button type="submit" name="submit" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="bottom" title="Restore"><span class="glyphicon glyphicon-refresh"></span></button>
          </form>
          <form action="{{ url('moderator/receipt/delete') }}" method="post" style="display: inline">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id" value="{{ $receipt->id }}">
            <button type="submit" name="submit" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="bottom" title="Hapus"><span class="glyphicon glyphicon-trash"></span></button>
          </form>
        </div>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>

{{ $receipts->links() }}

@stop
