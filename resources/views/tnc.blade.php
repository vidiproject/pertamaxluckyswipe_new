@extends('layouts.app')
@section('content')

<div class="container">
  <div class="row">
    <div class="col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4 text-center">

      <p><img class="img-responsive img-center" src="{{ url('img/image-lucky-swipe.png') }}" alt="Logo Lucky Swipe"></p>
      <div class="text-left">
        <h4 class="blue-title"><span style="font-size: 18px; font-weight: 600">Syarat & Ketentuan</span></h4>
      </div>

      <br>

      <div class="text-justify mekanisme-list">
        <ol style="padding-left: 15px">
          <li>Undian akan dilaksanakan sebanyak 2 Periode. Periode 1 dari 30 Januari 2017 s/d 30 Juni 2017. Periode 2 dari 1 Juli 2017 s/d 30 November 2017</li>
          <li>Pengundian akan dilakukan di depan Notaris, Perwakilan dari Departemen Sosial dan Kementerian Sosial pada waktu dan tempat yang akan ditetapkan oleh penyelenggara sebanyak 2 tahap.</li>

          <li>Pada undian Periode 1, 48 (empat puluh delapan) pemenang akan mendapatkan hadiah berupa 3 (tiga) unit Mobil Toyota Sienta Type G CVT (OTR) + Gratis BBM selama 1 tahun, 9 (sembilan) unit Motor Yamaha N-Max + gratis BBM selama 1 tahun, 21 (dua puluh satu) pemenang gratis BBM selama 1 tahun dan 15 (lima belas) unit Handphone iPhone 7.</li>

          <li>Pada undian periode 2, 54 (lima puluh empat) pemenang akan mendapatkan hadiah berupa 6 (enam) unit Mobil Toyota Sienta Type G CVT (OTR) + Gratis BBM selama 1 tahun, 9 (sembilan) unit Motor Yamaha N-Max + gratis BBM selama 1 tahun, 24 (dua puluh empat) pemenang gratis BBM selama 1 tahun dan 15 (lima belas) unit Handphone iPhone 7.</li>

          <li>Pajak hadiah undian ditanggung oleh penyelenggara program.</li>

          <li>Bagi pemenang yang belum berkesempatan menang di undian periode 1, akan berkesempatan untuk mendapatkan hadiah di periode berikutnya (dengan mengikuti program yang sama).</li>

          <li>Pemenang akan diumumkan di website dan media massa. Pemenang akan dihubungi oleh penyelenggara untuk pengaturan penyerahan hadiah.</li>

          <li>Seluruh Pemenang wajib memiliki identitas diri KTP, Kartu kredit atau debit yang didaftarkan dan Surat Konfirmasi menang dari Pertamina. Seluruh Pemenang wajib membawa bukti transaksi pada saat pengambilan hadiah.</li>

          <li>1 nomor telepon yang didaftarkan hanya berlaku untuk 1 peserta.</li>

          <li>Pemenang akan ditentukan melalui pengundian yang dilakukan di hadapan notaris. Keputusan pemenang undian adalah mutlak dan tidak dapat diganggu gugat.</li>

          <li>Pemenang tidak dapat memindahtangankan status kemenangannya kepada pihak lain.</li>

          <li>Hadiah tidak dapat diuangkan atau ditukarkan dalam bentuk apapun.</li>

          <li>Jika satu dan lain hal terjadi dan menyebabkan adanya perbedaan warna atau spesifikasi hadiah dibandingkan materi komunikasi yang diakibatkan ketidaktersediaan warna hadiah yang dimaksud, pemenang tidak dapat mengganggu gugat hal tersebut (disesuaikan dengan ketersediaan stok).</li>

          <li>Pemenang undian harus dapat dihubungi via telp untuk klarifikasi pada saat pengundian. Jika 3x tidak dapat dihubungi maka akan dianggap gugur.</li>

          <li>Apabila pemenang tidak mengambil hadiah dalam jangka waktu 90 (sembilan puluh) hari sejak dihubungi oleh penyelenggara sebagaimana penyelenggara telah mengirimkan surat pemberitahuan sebanyak 3 (kali) berturut-turut kepada pemenang, maka pemenang tersebut dianggap gugur sebagai pemenang Undian dan oleh karenanya pemenang undian tersebut tidak berhak untuk menerima hadiah. Panitia akan memilih kembali pemenang lainnya.</li>

          <li>Pemenang undian berhak mendapatkan hadiahnya jika memenuhi syarat data dan identitas yang telah ditetapkan oleh penyelenggara (identitas diri, Kartu kredit atau debit dan struk pembelian). Data pada microsite harus diisi lengkap &amp; sesuai identitas asli.</li>

          <li>Penyelenggara tidak bertanggung jawab atas kesalahaan data yang diberikan oleh pemenang. Pemenang wajib bekerjasama dan mentaati serta memenuhi seluruh persyaratan adminsitratif yang diperlukan untuk pengambilan hadiah sebagaimana diminta kepada pemenang oleh penyelenggara dan selama belum dipenuhinya persyaratan administratif tersebut atau belum menyediakan surat atau dokumen yang diperlukan untuk pengambilan hadiah maka penyelenggara berhak untuk menahan pemberian hadiah kepada pemenang. Penyelenggara berhak mendiskualifikasi pemenang yang tidak memenuhi syarat.</li>

          <li>Penyelenggara tidak bertanggung jawab atas terjadinya kecelakaan, kehilangan atau kerusakan yang diderita oleh pemenang sehubungan dengan penggunaan hadiah dalam program.</li>

          <li>Penyelenggara dapat mengubah, menghapus, atau menambahkan ketentuan dan persyaratan dan hal-hal yang dibutuhkan dalam program, yang akan diumumkan melalui website www.pertamaxluckyswipe.com.</li>

          <li>Syarat dan Ketentuan ini diatur berdasarkan hukum Indonesia. Sengketa yang timbul sehubungan dengan Syarat dan Ketentuan ini akan diselesaikan melalui Pengadilan Negeri Jakarta Selatan.</li>

          <li>Peserta program tidak akan dipungut biaya apapun untuk mengikuti program dan penyelenggara program tidak bertanggung jawab terhadap penipuan yang mengatasnamakan undian ini. Hati-hati terhadap segala jenis tindak penipuan yang mengatasnamakan PT Pertamina (Persero) atau Pihak Bank pendukung.</li>

          <li>Penyelenggara berhak membatalkan keputusan pemenang apabila tidak terpenuhinya persyaratan yang telah ditentukan dalam syarat dan ketentuan ini. Keputusan bersifat mutlak dan final serta tidak dapat diganggu gugat.</li>
          
          <li>Pemenang menyetujui bahwa penyelenggara tidak bertanggung jawab terhadap pembayaran tambahan apapun kepada pemenang selain dari hadiah yang telah ditetapkan dan pembayaran pajak hadiah.</li>
          
        </ol>
      </div>

      <br>

      <a href="{{ url('/home') }}" class="btn btn-primary triangle" style="width: 120px">Lanjut</a>

      <br><br>

    </div>
  </div>
</div>

@include('layouts.footer')

<!-- <footer>
  <div class="container-fluid">
    <div class="row">
      <div class="col-xs-10 col-xs-offset-1 col-md-4 col-md-offset-4">
          <ul class="bottom-menu">
            <li><a href="{{ url('/mekanisme') }}">Mekanisme</a></li>
            <li><a href="{{ url('/hadiah') }}">Hadiah</a></li>
            <li class="li-img"><a clas="phone-number" href="tel:+1500000">
              <img class="img-responsive" style="max-height: 50px" src="{{ url('img/hotline.png') }}" />
            </a></li>
          </ul>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-xs-7">
        <img class="img-responsive" src="{{ url('img/banks.png') }}" />
      </div>
      <div class="col-xs-2">

      </div>
      <div class="col-xs-3" style="padding-left: 0">
        <img class="img-responsive pull-right" src="{{ url('img/raise.png') }}" />
      </div>
    </div>
    <p><small>&#64; Hak Cipta 2016 &#45; 2017 PT Pertamina &#40;Persero&#41;</small></p>
  </div>
</footer> -->


@stop
