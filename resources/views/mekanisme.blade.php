@extends('layouts.app')
@section('stylesheet')
<style type="text/css">
  video {
    width: 100%    !important;
    height: auto   !important;
  }
</style>
@stop
@section('content')

<div class="container">
  <div class="row">
    <div class="col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4 text-center">

      <p><img class="img-responsive img-center" src="{{ url('img/image-lucky-swipe.png') }}" alt="Logo Lucky Swipe"></p>
      <div class="text-left">
        <h4 class="blue-title"><span style="font-size: 18px; font-weight: 600">Mekanisme</span></h4>
      </div>

      <br>

       <video width="400" height="300" poster="{{ asset('video/cover.jpg') }}" controls>
        <source src="{{ asset('video/video-mekanisme.mp4') }}" type="video/mp4">
        <!-- <source src="movie.ogg" type="video/ogg"> -->
        Your browser does not support the video tag.
      </video> 

      <br><br>

      <div style="text-align: justify">
        <ol class="mekanisme-list" style="padding-left: 15px">
          <li>Program berlaku dari tanggal 30 Januari 2017 sampai dengan 30 November 2017.</li>
          <li>Untuk mengikuti program, pelanggan perlu melakukan registrasi di website www.pertamaxluckyswipe.com.</li>
          <!-- <li>Setiap bulan pelanggan wajib melakukan akumulasi pembelian Pertamax Series, Dex Series dan Pertalite menggunakan kartu kredit atau debit Bank Mandiri, Bank BRI dan Bank BNI minimal Rp.500.000,- (lima ratus ribu rupiah) untuk mendapatkan 5 point. Setiap tambahan pembelian Rp.100.000,- (seratus ribu rupiah) akan mendapatkan tambahan 1 (satu) point.</li> -->
          <li>Pelanggan wajib melakukan akumulasi pembelian Pertamax Series, Dex Series, dan Pertalite menggunakan kartu kredit atau debit Bank Mandiri, Bank BRI, dan Bank BNI minimal Rp. 500.000,- (lima ratus ribu rupiah) pada pembelian pertama selama 1 periode pengundian untuk mendapat 1 nomor undian. Selanjutnya setiap pembelian dengan akumulasi total Rp. 100.000,- (seratus ribu rupiah) pelanggan akan mendapat 1 nomor undian berlaku kelipatan.</li>
          <li>Hitungan point dihitung dari 1 kartu kredit atau debit Bank tertentu tidak dapat digabungkan dengan point dari bank lain.</li>
          <li>Daftarkan diri anda dengan cara memasukkan data diri: nama, alamat email, no HP, no ktp, bank yang digunakan, dan data lain yang dibutuhkan.</li>
          <li>Data yang didaftarkan pada saat registrasi di website www.pertamaxluckyswipe.com harus sesuai dengan nama asli pada KTP dan juga nama kepemilikan kartu kredit atau debit.</li>
          <li>Setelah registrasi pelanggan akan mendapatkan nomor registrasi untuk ikut serta program Pertamina Fuel Lucky Swipe. Nomor registrasi disesuaikan dengan Bank yang didaftarkan.</li>
          <li>Pelanggan wajib upload 2 bukti: struk EDC &amp; struk Bensin pada setiap pembelian Pertamax series, Dex Series dan Pertalite, dimana informasi yang tertera dalam struk terlihat jelas (tidak buram,luntur, dll)</li>
          <li>Simpan struk pembelian Pertamax series, Dex Series dan Pertalite untuk validasi data pemenang.</li>
        </ol>
      </div>

      <br>

      <a href="{{ url('/home') }}" class="btn btn-primary triangle" style="width: 120px">Lanjut</a>

      <br><br>

    </div>
  </div>
</div>

<!-- <footer>
  <div class="container-fluid">
    <div class="row">
      <div class="col-xs-10 col-xs-offset-1 col-md-4 col-md-offset-4">
          <ul class="bottom-menu">
            <li><a href="{{ url('/mekanisme') }}">Mekanisme</a></li>
            <li><a href="{{ url('/hadiah') }}">Hadiah</a></li>
            <li class="li-img"><a clas="phone-number" href="tel:+1500000">
              <img class="img-responsive" style="max-height: 50px" src="{{ url('img/hotline.png') }}" />
            </a></li>
          </ul>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-xs-7">
        <img class="img-responsive" src="{{ url('img/banks.png') }}" />
      </div>
      <div class="col-xs-2">

      </div>
      <div class="col-xs-3" style="padding-left: 0">
        <img class="img-responsive pull-right" src="{{ url('img/raise.png') }}" />
      </div>

    </div>
    <p><small>&#64; Hak Cipta 2016 &#45; 2017 PT Pertamina &#40;Persero&#41;</small></p>
  </div>
</footer> -->

@include('layouts.footer')

@stop
