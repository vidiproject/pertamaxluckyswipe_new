@extends('layouts.app')
@section('content')

<div class="container">
  <div class="row">
    <div class="col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4 text-center">

      <p><img class="img-responsive img-center" src="{{ url('img/image-lucky-swipe.png') }}" alt="Logo Lucky Swipe"></p>
      <div class="text-left">
        <h4 class="blue-title"><span style="font-size: 18px; font-weight: 600">FAQ</span></h4>
      </div>

      <br>

      <div class="text-justify mekanisme-list">
        <div style="margin-bottom: 25px;">
          <p style="font-weight: bold;">Q. Berapa lama dan sampai kapan program Pertamina Fuel Lucky Swipe diadakan?</p>
          <p style="font-style: italic;"><strong>A.</strong> Program ini berjalan dari tanggal 30 Januari - 30 November 2017.</p>
        </div>
        <div style="margin-bottom: 25px;">
          <p style="font-weight: bold;">Q. Ada berapa periode dan kapan pelaksanaan undiannya?</p>
          <p style="font-style: italic;"><strong>A.</strong> Undian akan dilaksanakan sebanyak 2 Periode. Periode 1 dari tanggal 30 Januari 2017 - 30 Juni 2017, dan Periode 2 dari tanggal 1 Juli 2017 - 30 November 2017.</p>
        </div>
        <div style="margin-bottom: 25px;">
          <p style="font-weight: bold;">Q. Apa saja hadiahnya?</p>
          <p style="font-style: italic;"><strong>A.</strong> 9x Mobil Sienta Type G CVT + gratis BBM selama 1 tahun, 18x Motor N-Max + gratis BBM selama 1 tahun, 45x gratis BBM selama 1 tahun, dan 30x iPhone 7.</p>
        </div>
        <div style="margin-bottom: 25px;">
          <p style="font-weight: bold;">Q. Kartu apa saja yang berlaku dalam program Pertamina Fuel  Lucky Swipe?</p>
          <p style="font-style: italic;"><strong>A.</strong> Anda dapat menggunakan kartu kredit atau debit dari Bank Mandiri, Bank BNI, Bank BRI.</p>
        </div>
        <div style="margin-bottom: 25px;">
          <p style="font-weight: bold;">Q. Kalo ternyata EDC yang digunakan bukan dari ketiga bank tersebut apakah tetap bisa?</p>
          <p style="font-style: italic;"><strong>A.</strong> Anda tetap dapat ikut serta dalam program ini, asalkan kartu yang digunakan adalah kartu dari Bank Mandiri, BNI, dan BRI.</p>
        </div>
        <div style="margin-bottom: 25px;">
          <p style="font-weight: bold;">Q. Adakah minimal pembeliannya?</p>
          <p style="font-style: italic;"><strong>A.</strong> Untuk dapat ikut serta dalam program ini, dalam satu periode minimal Anda harus melakukan transaksi dengan akumulasi total Rp. 500,000.- (lima ratus ribu rupiah) untuk pembelian produk Pertamax Turbo, Pertamax, Pertalite, Pertamina Dex, dan Dexlite.</p>
        </div>
        <div style="margin-bottom: 25px;">
          <p style="font-weight: bold;">Q. Apakah poin akan hangus jika pembelian per bulan kurang dari batas yang telah ditentukan?</p>
          <p style="font-style: italic;"><strong>A.</strong> Poin tidak akan hangus, namun poin tidak akan dimasukan ke dalam pengundian.</p>
        </div>
        <div style="margin-bottom: 25px;">
          <p style="font-weight: bold;">Q. Jika pembelian melebihi batas yang telah ditentukan bagaimana?</p>
          <p style="font-style: italic;"><strong>A.</strong> Setiap tambahan pembelian dengan nominal Rp 100.000 akan mendapatkan 1 poin (berlaku kelipatan).</p>
        </div>
        <div style="margin-bottom: 25px;">
          <p style="font-weight: bold;">Q. Program Pertamina Fuel Lucky Swipe berlaku di mana saja?</p>
          <p style="font-style: italic;"><strong>A.</strong> Program ini berlaku di seluruh SPBU Pertamina di Indonesia yang menjual bahan bakar Pertamax Turbo, Pertamax, Pertalite, Pertamina Dex, dan Dexlite dan mempunyai mesin EDC.</p>
        </div>
        <div style="margin-bottom: 25px;">
          <p style="font-weight: bold;">Q. Struk apa saja yang perlu saya simpan?</p>
          <p style="font-style: italic;"><strong>A.</strong> Struk SPBU Pertamina dan struk dari mesin EDC.</p>
        </div>
        <div style="margin-bottom: 25px;">
          <p style="font-weight: bold;">Q. Setelah saya dapat struk, apa yang harus saya lakukan?</p>
          <p style="font-style: italic;"><strong>A.</strong> Lakukan registrasi / login di www.pertamaxluckyswipe.com, ikuti tahapannya, lalu foto dan upload struk-struk tersebut.</p>
        </div>
        <div style="margin-bottom: 25px;">
          <p style="font-weight: bold;">Q. Struknya tidak boleh hilang? Bagaimana jika hilang?</p>
          <p style="font-style: italic;"><strong>A.</strong> Kami sarankan struk langsung difoto dan diupload agar tidak hilang. Selama foto struk sudah masuk di website, berarti terhitung sah. Jika hilang sebelum diupload, maka harus melakukan pembelian ulang.</p>
        </div>
        <div style="margin-bottom: 25px;">
          <p style="font-weight: bold;">Q. Jika terdapat masalah dengan SPBU Pertamina harus menghubungi kemana?</p>
          <p style="font-style: italic;"><strong>A.</strong> Anda dapat menghubungi Pertamina Contact Centre (PCC) di 1500000.</p>
        </div>
        <div style="margin-bottom: 25px;">
          <p style="font-weight: bold;">Q. Jika terdapat masalah dengan website www.pertamaxluckyswipe.com harus menghubungi kemana?</p>
          <p style="font-style: italic;"><strong>A.</strong> Anda dapat menghubungi Pertamina Contact Centre (PCC) di 1500000, Instagram @pertamaxind, dan Facebook Pertamax IND.</p>
        </div>
        <div style="margin-bottom: 25px;">
          <p style="font-weight: bold;">Q. Jika terdapat masalah dengan kartu dan/atau mesin EDC bank harus menghubungi kemana?</p>
          <p style="font-style: italic;"><strong>A.</strong> Anda dapat menghubungi call centre masing-masing bank.</p>
        </div>
      </div>

      <!-- <div class="text-justify mekanisme-list">
        <ol style="padding-left: 15px">
          <li>
            <a href="#collapse1" data-toggle="collapse">Berapa lama dan sampai kapan program Pertamina Fuel Lucky Swipe diadakan?</a>
            <div class="collapse text-left" id="collapse1">
              <p style="font-style: italic;">Program ini berjalan dari tanggal 30 Januari - 30 November 2017.</p>
            </div>
          </li>
          <li>
            <a href="#collapse2" data-toggle="collapse">Ada berapa periode dan kapan pelaksanaan undiannya?</a>
            <div class="collapse text-left" id="collapse2">
              <p style="font-style: italic;">Undian akan dilaksanakan sebanyak 2 Periode. Periode 1 dari tanggal 30 Januari 2017 - 30 Juni 2017, dan Periode 2 dari tanggal 1 Juli 2017 - 30 November 2017.</p>
            </div>
          </li>
          <li>
            <a href="#collapse3" data-toggle="collapse">Apa saja hadiahnya?</a>
            <div class="collapse text-left" id="collapse3">
              <p style="font-style: italic;">9x Mobil Sienta Type G CVT + gratis BBM selama 1 tahun, 18x Motor N-Max + gratis BBM selama 1 tahun, 45x gratis BBM selama 1 tahun, dan 30x iPhone 7.</p>
            </div>
          </li>
          <li>
            <a href="#collapse4" data-toggle="collapse">Kartu apa saja yang berlaku dalam program Pertamina Fuel  Lucky Swipe?</a>
            <div class="collapse text-left" id="collapse4">
              <p style="font-style: italic;">Anda dapat menggunakan kartu kredit atau debit dari Bank Mandiri, Bank BNI, Bank BRI.</p>
            </div>
          </li>
          <li>
            <a href="#collapse5" data-toggle="collapse">Kalo ternyata EDC yang digunakan bukan dari ketiga bank tersebut apakah tetap bisa?</a>
            <div class="collapse text-left" id="collapse5">
              <p style="font-style: italic;">Anda tetap depat ikut serta dalam program ini, asalkan kartu yang digunakan adalah kartu dari Bank Mandiri, BNI, dan BRI.</p>
            </div>
          </li>
          <li>
            <a href="#collapse6" data-toggle="collapse">Adakah minimal pembeliannya?</a>
            <div class="collapse text-left" id="collapse6">
              <p style="font-style: italic;">Untuk dapat ikut serta dalam program ini, Anda harus melakukan transaksi sebesar Rp. 500,000.- (lima ratus ribu rupiah) per bulan (akumulatif per bulan) untuk pembelian produk Pertamax Turbo, Pertamax, Pertalite, Pertamina Dex, dan Dexlite.</p>
            </div>
          </li>
          <li>
            <a href="#collapse7" data-toggle="collapse">Apakah poin akan hangus jika pembelian per bulan kurang dari batas yang telah ditentukan?</a>
            <div class="collapse text-left" id="collapse7">
              <p style="font-style: italic;">Poin tidak akan hangus, namun poin tidak akan dimasukan ke dalam pengundian.</p>
            </div>
          </li>
          <li>
            <a href="#collapse8" data-toggle="collapse">Jika pembelian melebihi batas yang telah ditentukan bagaimana?</a>
            <div class="collapse text-left" id="collapse8">
              <p style="font-style: italic;">Setiap tambahan pembelian dengan nominal Rp 100.000 akan mendapatkan 1 poin (berlaku kelipatan).</p>
            </div>
          </li>
          <li>
            <a href="#collapse9" data-toggle="collapse">Program Pertamina Fuel Lucky Swipe berlaku di mana saja?</a>
            <div class="collapse text-left" id="collapse9">
              <p style="font-style: italic;">Program ini berlaku di seluruh SPBU Pertamina di Indonesia yang menjual bahan bakar Pertamax Turbo, Pertamax, Pertalite, Pertamina Dex, dan Dexlite dan mempunyai mesin EDC.</p>
            </div>
          </li>
          <li>
            <a href="#collapse10" data-toggle="collapse">Struk apa saja yang perlu saya simpan?</a>
            <div class="collapse text-left" id="collapse10">
              <p style="font-style: italic;">Struk SPBU Pertamina dan struk dari mesin EDC.</p>
            </div>
          </li>
          <li>
            <a href="#collapse11" data-toggle="collapse">Setelah saya dapat struk, apa yang harus saya lakukan?</a>
            <div class="collapse text-left" id="collapse11">
              <p style="font-style: italic;">Lakukan registrasi / login di www.pertamaxluckyswipe.com, ikuti tahapannya, lalu foto dan upload struk-struk tersebut.</p>
            </div>
          </li>
          <li>
            <a href="#collapse12" data-toggle="collapse">Struknya tidak boleh hilang? Bagaimana jika hilang?</a>
            <div class="collapse text-left" id="collapse12">
              <p style="font-style: italic;">Kami sarankan struk langsung difoto dan diupload agar tidak hilang. Selama foto struk sudah masuk di website, berarti terhitung sah. Jika hilang sebelum diupload, maka harus melakukan pembelian ulang.</p>
            </div>
          </li>
          <li>
            <a href="#collapse13" data-toggle="collapse">Jika terdapat masalah dengan SPBU Pertamina harus menghubungi kemana?</a>
            <div class="collapse text-left" id="collapse13">
              <p style="font-style: italic;">Anda dapat menghubungi Pertamina Contact Centre (PCC) di 1500000.</p>
            </div>
          </li>
          <li>
            <a href="#collapse14" data-toggle="collapse">Jika terdapat masalah dengan website www.pertamaxluckyswipe.com harus menghubungi kemana?</a>
            <div class="collapse text-left" id="collapse14">
              <p style="font-style: italic;">Anda dapat menghubungi Pertamina Contact Centre (PCC) di 1500000, Instagram @pertamaxind, dan Facebook Pertamax IND.</p>
            </div>
          </li>
          <li>
            <a href="#collapse15" data-toggle="collapse">Jika terdapat masalah dengan kartu dan/atau mesin EDC bank harus menghubungi kemana?</a>
            <div class="collapse text-left" id="collapse15">
              <p style="font-style: italic;">Anda dapat menghubungi call centre masing-masing bank.</p>
            </div>
          </li>
        </ol>
      </div> -->

      <br><br>

    </div>
  </div>
</div>
@include('layouts.footer')
@stop
