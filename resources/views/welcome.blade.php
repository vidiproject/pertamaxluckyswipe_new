@extends('layouts.app')
@section('content')

<div class="container">
  <div class="row">
    <div class="col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4 text-center">

      <p><img class="img-responsive img-center" src="{{ url('img/image-lucky-swipe.png') }}" alt="Logo Lucky Swipe"></p>
      <h4 class="blue-title"><span class="title-top" style="font-weight: 600">Bayar dengan Kartu,</span> <span style="font-weight: 600; font-size: 20px;"><br>Kini Bebas Biaya Tambahan</span></h4>
      <p>
        Menangkan berbagai hadiah spesial dengan mengumpulkan poin dari setiap pembelian Pertamax Series, Dex Series, Pertalite
      </p>
      <p><img class="img-responsive img-center" src="{{ url('img/image-hadiah-1.png') }}" alt="Image Hadiah"></p>
      <br>
      <p><small style="font-size: 70%">*Bagi pengguna desktop dan smartphone, pastikan Anda sudah melakukan scan atau foto pada kedua struk untuk dapat di upload.</small></p>
      <br>
      <div class="text-center">
        @if(Auth::check())
        <a class="btn btn-primary triangle" href="{{ url('/home/upload-struk') }}" style="width: 140px;">Upload Struk</a>
        @else
        <a class="btn btn-primary triangle" href="{{ url('login') }}" style="width: 140px;">Masuk / Daftar</a>
        @endif
      </div>
    </div>
  </div>
</div>

<!-- Cache::forget('key'); -->

@if (!Auth::check())
<!-- Modal -->
<div class="modal intro_modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content text-center">
      <div class="modal-header">
        <button type="button" class="close" onclick="setCookies();" data-dismiss="modal" aria-label="Close"><i class="material-icons">clear</i></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-10 col-md-offset-1">
            <h4 class="blue-title"><span class="title-top" style="font-weight: 600">Bayar dengan Kartu,</span><br><span style="font-weight: 600; font-size: 20px;">Kini Bebas Biaya Tambahan</span></h4>
            <br>
            <p>Hubungi Pertamina Call Center [PCC] di nomor 1 500 000 apabila pembayaran dengan kartu masih dikenakan biaya tambahan di SPBU Pertamina</p>
            <br>
            <img class="img-responsive img-center" src="{{ url('img/image-lucky-swipe.png') }}" alt="Logo Lucky Swipe">
            <br>
          </div>
        </div>
      </div>
      <div class="modal-footer text-center" style="text-align:center!important">
        <a href="#" onclick="setCookies();" data-dismiss="modal" class="btn btn-primary triangle" style="width: 120px">Mulai</a>
        <br><br><br>
      </div>
    </div>
  </div>
</div>
@endif

@include('layouts.footer')

@stop
@section('script')
<script type="text/javascript">
  $('.intro_modal').modal('show');
  // function setCookies()
  // {
  //   document.cookie = "modal=close";
  //   var x = document.cookie;
  //   console.log(x);
  // }

  // function getCookie(cname) {
  //   var name = cname + "=";
  //   var ca = document.cookie.split(';');
  //   for(var i = 0; i < ca.length; i++) {
  //       var c = ca[i];
  //       while (c.charAt(0) == ' ') {
  //           c = c.substring(1);
  //       }
  //       if (c.indexOf(name) == 0) {
  //           return c.substring(name.length, c.length);
  //       }
  //   }
  //   return "";
  // }

  // var user = getCookie("modal");
  // if (user == "") {
  //   $('.intro_modal').modal('show');
  // }  
  
</script>
@stop
