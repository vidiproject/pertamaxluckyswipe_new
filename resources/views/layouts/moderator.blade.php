<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Pertamina') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/moderator.css') }}">

    <style type="text/css">
      .pagination{
        margin: 0;
        margin-bottom: 15px;
        float: right;
      }
    </style>

    <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>

    <script type="text/javascript">
    function setCookie(cname,cvalue,exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires=" + d.toGMTString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    // function checkCookie() {
    //     var user = getCookie("previous_url");
    //     if (user != "") {
    //         alert("Url : " + user);
    //     } else {
    //        var user = '<?php echo url()->previous(); ?>';
    //        if (user != "" && user != null) {
    //            setCookie("previous_url", user, 30);
    //        }
    //     }
    // }

    function callMeMaybe()
    {
        var url = '<?php echo Request::fullUrl(); ?>';
        if (url != "" && url != null) {
           setCookie("previous_url", url, 30);
        }
    }
    </script>
</head>
<body>

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="{{ url('/moderator/check') }}" style="text-transform: capitalize;">
        {{ Auth::user()->handle }}
      </a>
    </div>
    <div class="navbar-header pull-right">
      @if (Auth::check())
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#" onclick="event.preventDefault();
               document.getElementById('logout-form').submit();">Logout</a>
            </li>
          </ul>
        </li>
      </ul>
      
      @endif
      <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
         {{ csrf_field() }}
      </form>
    </div>
  </div>
</nav>

<div class="container-fluid">
  <div class="col-md-1">
    <div class="list-group">
      <a href="{{ url('/moderator') }}" class="list-group-item text-center {{ ( Request::is('moderator') ? 'active' : '' ) }}" title="Dashboard"><span class="glyphicon glyphicon-dashboard" aria-hidden="true"></span></a>
      <a href="{{ url('/moderator/check') }}" class="list-group-item text-center {{ ( Request::is('moderator/check') ? 'active' : '' || Request::is('moderator/check/*') ? 'active' : '' ) }}" title="Moderation"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></a>

      <a href="{{ url('/moderator/approval') }}" class="list-group-item text-center {{ ( Request::is('moderator/approval') ? 'active' : '' || Request::is('moderator/approval/*') ? 'active' : '' ) }}" title="Approval"><span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></a>

      <a href="{{ url('/moderator/participant') }}" class="list-group-item text-center {{ ( Request::is('moderator/participant') ? 'active' : '' ) }}" title="Participant"><span class="glyphicon glyphicon-user" aria-hidden="true"></a>

      <a href="{{ url('/moderator/data') }}" class="list-group-item text-center {{ ( Request::is('moderator/data') ? 'active' : '' ) }}" title="Export"><span class="glyphicon glyphicon-save-file" aria-hidden="true"></a>
      @if(Auth::user()->handle == 'bni')
        <img src="{{ url('/img/bni.jpg') }}" class="img-responsive list-group-item">
      @elseif(Auth::user()->handle == 'bri')
        <img src="{{ url('/img/bri.jpg') }}" class="img-responsive list-group-item">
      @else
        <img src="{{ url('/img/mandiri.jpg') }}" class="img-responsive list-group-item">
      @endif      
    </div>
  </div>
  <div class="col-md-11">
    <div class="panel panel-default">
      <div class="panel-body">
        @yield('content')
      </div>
    </div>
  </div>
</div>

@yield('script')

</body>
</html>
