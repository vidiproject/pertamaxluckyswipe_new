<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="format-detection" content="telephone=yes">
    <title>{{ config('app.name', 'Pertamina Fuel Lucky Swipe') }}</title>
    <meta name="description" content="Raih hadiah spesial dengan membeli Pertamax Series, Dex series, dan Pertalite menggunakan kartu debit atau kredit dari bank BRI, BNI, dan Mandiri.">
    <meta name="google-site-verification" content="ZQ7FwC76C-bLZcLpCTfcbCp3RqesY1seLOJToXM7nlM" />

    <!-- Styles -->
    <!-- <link href="//db.onlinewebfonts.com/c/0f56c874ba695191e7f4cab237447698?family=Futura" rel="stylesheet" type="text/css"/> -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> 
    <link rel="stylesheet" href="{{ url('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ url('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('css/custom.css') }}">
    @yield('stylesheet')

    <!-- Scripts -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-91078330-1', 'auto');
        ga('send', 'pageview');
    </script>

    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window,document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
     fbq('init', '1359657677442686'); 
    fbq('track', 'PageView');
    </script>
    <noscript>
     <img height="1" width="1" src="https://www.facebook.com/tr?id=1359657677442686&ev=PageView&noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->
    
</head>
<body>

<div class="main" id="main">

    <div id="mySidenav" class="sidenav">
      <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><i class="material-icons">clear</i></a>

      <a href="{{ url('/') }}"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Home</a>
      <div style="padding: 8px 32px 8px 32px;">
        <div style="height: 1px; background-color: red;"></div>
      </div>

      <a href="{{ url('/syarat-dan-ketentuan') }}"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Syarat &amp; Ketentuan</a>
      <div style="padding: 8px 32px 8px 32px;">
        <div style="height: 1px; background-color: red;"></div>
      </div>

      <a href="{{ url('/faq') }}"><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span> FAQ</a>
      <div style="padding: 8px 32px 8px 32px;">
        <div style="height: 1px; background-color: red;"></div>
      </div>

      <a href="{{ url('/mekanisme') }}"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Mekanisme</a>
      <div style="padding: 8px 32px 8px 32px;">
        <div style="height: 1px; background-color: red;"></div>
      </div>

      <a href="{{ url('/hadiah') }}"><span class="glyphicon glyphicon-gift" aria-hidden="true"></span> Hadiah</a>
      <div style="padding: 8px 32px 8px 32px;">
        <div style="height: 1px; background-color: red;"></div>
      </div>

      <a href="{{ url('/home') }}"><span class="glyphicon glyphicon-star" aria-hidden="true"></span> Poin</a>
      <div style="padding: 8px 32px 8px 32px;">
        <div style="height: 1px; background-color: red;"></div>
      </div>

      @if (Auth::check())
          <a href="#" onclick="event.preventDefault();
                 document.getElementById('logout-form').submit();"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Logout</a>
          <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
             {{ csrf_field() }}
          </form>
      @endif
    </div>

    <nav class="navbar navbar-default">
      <div class="container-fluid" style="padding-right: 0; padding-left: 0">
        <div class="none">
          <div class="col-xs-4 col-sm-5 col-md-8" style="padding-right: 0; padding-left: 0">
            <div class="navbar-header" style="float: left">
              <button type="button" onclick="openNav()" class="navbar-toggle visible-xs visible-sm hidden-md hidden-lg" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div>
            <div class="navbar-collapse hidden-xs hidden-sm visible-md visible-lg">
              <ul class="nav navbar-nav">
                <li><a href="{{ url('/') }}"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Home</a></li>
                <li><a href="{{ url('/mekanisme') }}"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Mekanisme</a></li>
                <li><a href="{{ url('/syarat-dan-ketentuan') }}"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Syarat  & Ketentuan</a></li>
                <li><a href="{{ url('/faq') }}"><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span> FAQ</a></li>
                <li><a href="{{ url('/hadiah') }}"><span class="glyphicon glyphicon-gift" aria-hidden="true"></span> Hadiah</a></li>
                <li><a href="{{ url('/home') }}"><span class="glyphicon glyphicon-star" aria-hidden="true"></span> Poin</a></li>
                @if (Auth::check())
                <li><a href="#" onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Logout</a></li>
               @endif
              </ul>
            </div><!-- /.navbar-collapse -->
          </div>
          <div class="col-xs-2 col-sm-2 col-md-2" style="padding-right: 0; padding-left: 0; text-align: right">
            <div class="navbar-header" style="float: right">
              <a class="navbar-brand" href="#"><img class="img-responsive logo-bumn pull-right" src="{{ url('img/logo-bumn.png') }}" slt="Logo BUMN" style="float:right" /></a>
            </div>
          </div>
          <div class="col-xs-6 col-sm-5 col-md-2" style="padding-right: 0; padding-left: 0">
            <div class="navbar-header" style="float: right">
              <a class="navbar-brand" href="{{ url('/') }}"><img class="img-responsive logo-pertamina" src="{{ url('img/logo-pertamina.png') }}" alt="Logo Pertamina" /></a>
            </div>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </nav>

    @yield('content')

    </div><!-- main -->
    <div class="modal-backdrop fade in" style="display: none"></div>

    <!-- Scripts -->
    <script type="text/javascript" src="{{ url('js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript">

    function openNav() {
        document.getElementById("mySidenav").style.width = "250px";
        var bd = $('.modal-backdrop');
        bd.css('display','block');
    }

    /* Set the width of the side navigation to 0 */
    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        var bd = $('.modal-backdrop');
        bd.fadeOut(100, function () {
            $(this).css({display:"none"});
        });
    }

    </script>

    @yield('script')

</body>
</html>
