<br>
<footer>
  <div class="container-fluid">
    <div class="row">
      <div class="col-xs-9 visible-xs">
        <img class="img-responsive" src="{{ url('img/banks2.png') }}" />
      </div>
      <div class="col-xs-3 col-sm-12 col-md-12">
        <a clas="phone-number" href="tel:1500000">
          <img class="img-responsive pull-right" style="max-height: 50px" src="{{ url('img/hotline.png') }}" />
        </a>
        <img class="img-responsive hidden-xs" src="{{ url('img/banks2.png') }}" style="float: right; margin-right: 15px; max-height: 25px; " />
      </div>
    </div>
    <p><small>&#64; Hak Cipta 2016 &#45; 2017 PT Pertamina &#40;Persero&#41;</small></p>
  </div>
</footer>
