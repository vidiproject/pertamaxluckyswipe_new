@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4 text-center">

          <p><img class="img-responsive img-center" src="{{ url('img/image-lucky-swipe.png') }}" alt="Logo Lucky Swipe"></p>
          <h4 class="blue-title"><span style="font-size: 32px; font-weight: 600">Bayar dengan Kartu,</span> <span style="font-weight: 600"><br>Kini Bebas Biaya Tambahan</span></h4>
          <br>
          <form method="post" action="{{ url('home/edit') }}">
                <div class="form-group text-left {{ ( $errors->has('name') ? 'has-error' : '' ) }}">
                    <label>Nama</label>
                    <input type="text" name="name" class="form-control" value="{{ ( old('name') == '' ? $user->name : old('name') ) }}">
                    @if($errors->has('name'))
                    <span class="help-block">
                        <p>{{ $errors->first('name') }}</p>
                    </span>
                    @endif
                </div>

                <div class="form-group text-left {{ ( $errors->has('email') ? 'has-error' : '' ) }}">
                    <label>Email</label>
                    <input type="email" name="email" class="form-control" value="{{ ( old('email') != '' ? old('email') : $user->email ) }}">
                    @if($errors->has('email'))
                    <span class="help-block">
                        <p>{{ $errors->first('email') }}</p>
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <br>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" value="{{ $user->id }}">
                    <button class="btn btn-primary triangle" type="submit">SIMPAN</button>
                </div>
          </form>

        </div>
    </div>
</div>
@include('layouts.footer')
@endsection