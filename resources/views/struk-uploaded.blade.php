@extends('layouts.app')
@section('content')

<div class="container">
  <div class="row">
    <div class="col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4 text-center">
      <br><br>
      <h3 style="font-size: 42px; color: #666">SELAMAT!</h3>

      <p style="color: #666">Data transaksi dan foto struk sudah berhasil tersimpan & poin sudah bertambah.</p>
      <p style="color: #666">Ayo tingkatkan terus poin untuk meraih berbagai hadiah spesial.</p>
      <br><br><br><br>
      <a href="{{ url('/home') }}" class="btn btn-primary triangle" style="width: 150px">Kembali</a>

    </div>
  </div>
</div>
@include('layouts.footer')
@stop
