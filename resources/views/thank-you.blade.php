@extends('layouts.app')
@section('content')

<div class="container">
  <div class="row">
    <div class="col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4 text-center">

      <p><img class="img-responsive img-center" src="{{ url('img/image-lucky-swipe.png') }}" alt="Logo Lucky Swipe"></p>
      <h4 class="blue-title"><span style="font-size: 32px; font-weight: 600">Bayar dengan Kartu,</span> <span style="font-weight: 600"><br>Kini Bebas Biaya Tambahan</span></h4>

      <br>

      <p style="font-size: 20px">Selamat! kamu berhasil melakukan registrasi dan telah terdaftar sebagai peserta program Pertamina Fuel Lucky Swipe.</p>

      <p>Ayo upload struk pembelian Pertamax Series, Dex Series, dan Pertalite menggunakan kartu debit atau kredit sebanyak-banyaknya untuk meningkatkan poin undian!</p>

      <br>

      <a href="{{ url('/home') }}" class="btn btn-primary triangle">Lanjutkan</a>

      <br><br>

    </div>
  </div>
</div>

@include('layouts.footer')
@stop
