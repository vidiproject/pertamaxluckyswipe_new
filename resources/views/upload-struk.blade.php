@extends('layouts.app')
@section('content')
<div id="loader" style="display: none"></div>
<div class="container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4 text-center">

          <p><img class="img-responsive img-center" src="{{ url('img/image-lucky-swipe.png') }}" alt="Logo Lucky Swipe"></p>
          <p>Pastikan data terisi dengan lengkap dan benar</p>

          <br>

          @if (!$errors->isEmpty())
              <span class="help-block error-text">
                <i class="fa fa-exclamation-circle" aria-hidden="true"></i> Mohon periksa kembali data anda
              </span>
          @endif

          <form method="post" action="{{ url('home/upload-struk-store') }}" class="text-left">
              <div class="row">

                <div class="col-xs-12">
                  <label for="nominal" class="{{ $errors->has('bank') ? 'has-error' : '' }}">Pilih jenis kartu kredit / debit bank</label>
                </div>
                <div class="col-xs-12" style="padding-left: 7px; padding-right: 7px;">
                  <label class="label-image form-group col-xs-4 padding-lr-5">
                    <input {{ ( old('bank') == 'mandiri' ? 'checked' : '' ) }} type="radio" name="bank" value="mandiri" />
                    <img class="img-responsive" src="{{ url('img/mandiri.jpg') }}">
                  </label>
                  <label class="label-image form-group col-xs-4 padding-lr-5">
                    <input {{ ( old('bank') == 'bni' ? 'checked' : '' ) }} type="radio" name="bank" value="bni" />
                    <img class="img-responsive" src="{{ url('img/bni.jpg') }}">
                  </label>
                  <label class="label-image form-group col-xs-4 padding-lr-5">
                    <input {{ ( old('bank') == 'bri' ? 'checked' : '' ) }} type="radio" name="bank" value="bri" />
                    <img class="img-responsive" src="{{ url('img/bri.jpg') }}">
                  </label>
                </div>
              </div>

              <div class="row">
                <div class="col-xs-12">
                  <label for="nominal" class="{{ $errors->has('payment') ? 'has-error' : '' }}">Pilih pembayaran</label>
                </div>
                <div class="col-xs-12" style="padding-left: 7px; padding-right: 7px;">
                  <label class="label-image col-xs-6 padding-lr-5">
                    <input onclick="javascript:yesnoCheck();" type="radio" name="payment" id="noCheck" value="debit" {{ ( old('payment') == 'debit' ? 'checked' : '' ) }} />
                    <img class="img-responsive" src="{{ url('img/debit.jpg') }}">
                  </label>
                  <label class="label-image col-xs-6 padding-lr-5">
                    <input onclick="javascript:yesnoCheck();" type="radio" name="payment" id="yesCheck" value="kredit" {{ ( old('payment') == 'kredit' ? 'checked' : '' ) }} />
                    <img class="img-responsive" src="{{ url('img/kredit.jpg') }}">
                  </label>
                </div>
              </div>

              <br>

            <div class="row">
              <div class="form-group col-xs-12{{ $errors->has('nocc') ? ' has-error' : '' }}">
                <label class="control-label" for="nominal">Masukan 6 digit awal kartu kredit / debit</label>
                <input class="form-control" id="nocc" name="nocc" type="text" placeholder="Contoh 456747" class="validate" value="{{ old('nocc') }}">
              </div>
            </div>

            <div class="row">
              <div class="form-group col-xs-12{{ $errors->has('nominal') ? ' has-error' : '' }}">
                <label class="control-label" for="nominal">Masukan nominal pembelian struk bensin</label>
                <input id="nominal" class="form-control" name="nominal" type="text" placeholder="Contoh 750000" class="validate" value="{{ old('nominal') }}">
              </div>
            </div>

            <div class="row">
              <div class="form-group col-xs-12{{ $errors->has('nospbu') ? ' has-error' : '' }}">
                <label class="control-label" for="nominal">Masukan nomor SPBU</label>
                <input id="nominal" class="form-control" name="nospbu" type="text" placeholder="Contoh 3115301 (penulisan tanpa tanda baca)" class="validate" value="{{ old('nospbu') }}">
              </div>
            </div>

            <div class="row">
              <div class="col-xs-12 {{ Session::has('error') ? 'has-error' : '' }}">
                <label class="control-label" for="nominal">Upload bukti pembayaran</label>
              </div>
              <div class="col-xs-6" style="padding-right: 5px;">
                <div class="btnGabungan {{ Session::has('error') ? 'has-error' : '' }}">
                  <a href="#" class="btnUploadStruk" id="btnUploadStrukTop">
                    <img id="imgStruk1" src="{{ ( Session::has('img1') ? Session::get('img1') : url('img/icon-struk-pertamina.png') ) }}" class="img-responsive imgUploaded" style="min-width:100%" alt="">
                  </a>
                  <div class="additionalBtnUpload" id="btnUploadStruk">
                    <i class="fa fa-camera" aria-hidden="true"></i>
                    <a href="#">Upload bukti pembayaran Pertamina</a>
                  </div>
                </div>
              </div>
              <div class="col-xs-6" style="padding-left: 5px;">
                <div class="btnGabungan {{ Session::has('error') ? 'has-error' : '' }}">
                  <a href="#" class="btnUploadStruk2" id="btnUploadStruk2Top">
                    <img id="imgStruk2" src="{{ ( Session::has('img2') ? Session::get('img2') : url('img/icon-struk-atm.png') ) }}" class="img-responsive imgUploaded" style="min-width:100%" alt="">
                  </a>
                  <div class="additionalBtnUpload" id="btnUploadStruk2">
                    <i class="fa fa-camera" aria-hidden="true"></i>
                    <a href="#"> Upload bukti pembayaran bank</a>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 text-center">
                <br>
                <i><small id="fileWarning" style="font-size: 12px;">Ukuran file maksimal 5-MB dengan format .jpg atau .png.</small></i>
              </div>
            </div>

            <input type="hidden" name="id" value="{{ Auth::id() }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row text-center">
              <br>
              <button type="submit" class="btn btn-primary triangle" name="button" style="width: 120px;">Kirim</button>
            </div>
          </form>

           <form id="formUploadStruk" action="{{ url('home/upload-struk-a') }}" method="post" enctype="multipart/form-data" style="display: none">
              <input type="hidden" name="_token" value="{{ csrf_token() }}"></input>
              <input id="struk" type="file" class="form-control" name="photo" accept="image/*">
              <input type="hidden" name="id" value="{{ Auth::id() }}"></input>
              <input type="submit" value="Submit"></input>
           </form>

           <form id="formUploadStruk2" action="{{ url('home/upload-struk-b') }}" method="post" enctype="multipart/form-data" style="display: none">
              <input type="hidden" name="_token" value="{{ csrf_token() }}"></input>
              <input id="struk2" type="file" class="form-control" name="photo" accept="image/*">
              <input type="hidden" name="id" value="{{ Auth::id() }}"></input>
              <input type="submit" value="Submit"></input>
           </form>

        </div>
    </div>
</div>
<div id="myModal" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body text-center">
        <p class="text-danger" style="font-size: 12px;"><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Ukuran file maksimal 5-MB dengan format .jpg atau .png</p>
      </div>
      <div class="modal-footer" style="text-align: center!important; ">
        <button type="button" class="btn btn-primary triangle" data-dismiss="modal">Coba lagi</button>
      </div>
    </div>
  </div>
</div>
@include('layouts.footer')
@endsection
@section('script')
<script type="text/javascript" src="{{ url('js/jquery-form.js') }}"></script>
<script type="text/javascript">
// upload struk
$("#btnUploadStruk").click(function() {
    $("#struk").click();
});

$("#btnUploadStrukTop").click(function() {
    $("#struk").click();
});

$('#struk').change(function() {
      var status = $('#status');
      console.log('start!');
      $('#formUploadStruk').ajaxForm({
          beforeSend: function() {
              status.empty();
          },
          uploadProgress: function(event, position, total, percentComplete) {
              console.log('uploading...');
              $('#loader').css('display','block');
              var bd = $('.modal-backdrop');
              bd.css('display','block');
              //$('#uploaded_logo').attr('src', "{{ url('lib/img/preloader.gif') }}");
          },
          complete: function(xhr) {
            console.log(xhr.responseText.replace(/\"|\\/g, ""));
            var ct = xhr.getResponseHeader("content-type") || "";
            if (ct.indexOf('text') > -1) {
              //do something
              var x = xhr.responseText.replace(/\"|\\/g, "");
              var newSrc = $('#imgStruk1').attr('src').replace( $('#imgStruk1').attr('src'), x);
              $("#imgStruk1").attr("src", x);
              $('#loader').css('display','none');
              var bd = $('.modal-backdrop');
              bd.css('display','none');
            }
            if (ct.indexOf('json') > -1) {
              // handle json here
              var x = xhr.responseText.replace(/\"|\\/g, "");
              $('#myModal').modal('show');
              $('#loader').css('display','none');
              var bd = $('.modal-backdrop');
              bd.css('display','none');
            }
          }
      });

      $('#formUploadStruk').submit();
});

// upload struk
$("#btnUploadStruk2").click(function() {
    $("#struk2").click();
});

$("#btnUploadStruk2Top").click(function() {
    $("#struk2").click();
});

$('#struk2').change(function() {
      var status = $('#status');
      console.log('start!');
      $('#formUploadStruk2').ajaxForm({
          beforeSend: function() {
              status.empty();
          },
          uploadProgress: function(event, position, total, percentComplete) {
              console.log('uploading...');
              $('#loader').css('display','block');
              var bd = $('.modal-backdrop');
              bd.css('display','block');
              //$('#uploaded_logo').attr('src', "{{ url('lib/img/preloader.gif') }}");
          },
          complete: function(xhr) {
              console.log('uploaded!');
              console.log(xhr.responseText.replace(/\"|\\/g, ""));
              var x = xhr.responseText.replace(/\"|\\/g, "");
              var newSrc = $('#imgStruk2').attr('src').replace( $('#imgStruk2').attr('src'), x);
              $("#imgStruk2").attr("src", x);
              $('#loader').css('display','none');
              var bd = $('.modal-backdrop');
              bd.css('display','none');
          }
      });

      $('#formUploadStruk2').submit();
});


// function yesnoCheck() {
//   if (document.getElementById('yesCheck').checked) {
//       document.getElementById('ifYes').style.display = 'block';
//   }
//   else document.getElementById('ifYes').style.display = 'none';
// }

</script>
@stop
