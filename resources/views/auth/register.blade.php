@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">

      <p><img class="img-responsive img-center" src="{{ url('img/image-lucky-swipe.png') }}" alt="Logo Lucky Swipe"></p>
      <h4 class="blue-title text-center"><span style="font-size: 32px; font-weight: 600">Bayar dengan Kartu,</span> <span style="font-weight: 600"><br>Kini Bebas Biaya Tambahan</span></h4>

      <br>

      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation"><a class="triangle-white" href="{{ url('login') }}" aria-controls="login" role="tab">Masuk</a></li>
        <li role="presentation" class="active triangle"><a href="{{ url('register') }}" aria-controls="register" role="tab">Daftar</a></li>
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="login">
          <form role="form" method="POST" action="{{ url('/register') }}">
              {{ csrf_field() }}

              <div class="row text-center">
                @if (!$errors->isEmpty())
                    <span class="help-block error-text">
                        <i class="fa fa-exclamation-circle" aria-hidden="true"></i> Mohon data diperiksa kembali
                    </span>
                @endif
              </div>

              <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                  <label for="name" class="control-label">Nama Lengkap</label>
                  <input id="name" type="text" class="form-control" name="nama" value="{{ old('nama') }}">
              </div>

              <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                  <label for="email" class="control-label">Email</label>
                  <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
              </div>

              <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                  <label for="password" class="control-label">Kata sandi</label>
                  <input id="password" type="password" class="form-control" name="password" placeholder="Gunakan spesial karakter (pertamina123)">
              </div>

              <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                  <label for="password-confirm" class="control-label">Ulangi Kata sandi</label>
                  <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
              </div>

              <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                  <label for="phone" class="control-label">No Handphone</label>
                  <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" placeholder="Contoh 081234567898">
              </div>

              <div class="form-group{{ $errors->has('ktp') ? ' has-error' : '' }}">
                  <label for="ktp" class="control-label">No KTP</label>
                  <input id="ktp" type="text" class="form-control" name="ktp" value="{{ old('ktp') }}">
              </div>

              <div class="form-group text-center">
                  <br>
                  <button type="submit" class="btn btn-primary triangle">
                      Lanjutkan
                  </button>
              </div>
          </form>
        </div>
        <div role="tabpanel" class="tab-pane" id="register">
          <!-- nothing is here -->
        </div>
      </div>
    </div>
  </div>
</div>

@include('layouts.footer')
@endsection
@section('script')
<script type="text/javascript" src="{{ url('js/input-mask.js') }}"></script>
<script type="text/javascript">
  $(":input").inputmask(); // phone number mask
</script>
@stop
